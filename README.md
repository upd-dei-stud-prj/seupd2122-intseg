![Alt text](./homework-1/figure/INTSEG-logos_resized.jpeg?raw=true"INTSEG logo")
# INTSEG - Touchè Task 1

This repository is carried out by group INTSEG at the University of Padua as a homework of the [Search Engines](https://iiia.dei.unipd.it/education/search-engines/) course.
The homework consists of participating in one of the labs organized yearly by [CLEF](https://www.clef-initiative.eu/) (Conference and Labs of the Evaluation Forum) which in our case is the Touchè: Argument Retrieval lab.

## Group members

| Surname       | Name          | ID            |
| ------------- | ------------- | ------------- |
| Bahrami	| Sepide	| 2043887	|
| Goli		| Gnana Prakash	| 2048695	|
| Pasin		| Andrea	| 2041605       |
| Rajkumari	| Neemol	| 2008502	|
| Sohail        | Mohammad Muzammil	| 2043886	|
| Tahan		| Paria		| 2043889	|

## Organisation of the repository ###

The repository is organised as follows:

* `code`: This folder contains the source code of the developed system.
    * `main`
        * `java`
            * `it.unipd.dei.se`
                * `analyzer`
                    * `AnalyzerUtil.java`
                    * `IntsegAnalyzer.java`
                * `indexer`
                    * `ContextField.java`
                    * `DirectoryIndexer.java`
                    * `SentenceIDField.java`
                    * `SentenceValueField.java`
                    * `StanceField.java`
                * `parser`
                    * `DocumentParser.java`
                    * `IntsegParser.java`
                    * `ParsedDocument.java`
                * `searcher`
                    * `ISearcher.java`
                    * `TokenWeightTag.java`
                * `tools`
                    * `AnalyzerParams.java`  
                    * `ApacheNLPManager.java`
                    * `Constants.java`
                    * `DebaterManager.java`
                    * `ResourcesUtil.java`
                    * `SearcherParams.java`
                    * `TermWeightingUtils.java`
                    * `WeightParams.java`
                    * `WordNetManager.java`
                * `IntsegSearchEngine.java`
        * `resources`
            * `AnalyzerParams.xml` 
            * `context_stats.txt`
            * `en-pos-maxent.bin`
            * `en-pos-perceptron.bin`
            * `java_api_project_debater.jar`
            * `langdetect-183.bin`
            * `ordered-sentiment-words.txt`
            * `SearcherParams.xml`
            * `sentences_stats.txt`
            * `SentiWordNet_3.0.0.txt`
            * `stoplist-top100-context.txt`
            * `stoplist-top100-sentence.txt`
            * `synonyms.csv`
            * `topics.xml`
            * `topics-task-1-only-titles.xml`
            * `WeightParams.xml`
* `homework-1`: This folder contains the report describing the techniques applied and insights gained.
* `homework-2`: This folder contains the final paper submitted to CLEF.
* `final_jar_executable`: This folder contains the jar executable version of our system.
    * `INTSEG_SearchEngine-1.0-jar-with-dependencies.jar`: the jar executable version of our system.
* `results`: This folder contains the performance scores of the runs.
* `runs`: This folder contains the runs produced by the developed system.
* `slides`: This folder contains the slides used for presenting the conducted project.

## Project Description ##

This project aims at retrieving a pair of coherent sentences through an online debate portals dataset which are related to a specific topic. The corpora can be found on [Touchè Task 1](https://webis.de/events/touche-22/shared-task-1.html) website and evaluation topics are available at the resources folder under the name "topics.xml" for 2022 topics and "topics-task-1-only-titles.xml" for those from previous year.

As it can be seen in the project structure, 4 main parts of the code are the **analyzer**, **indexer**, **parser** and the **searcher**. What basically happens during the runtime is that, the input corpus is parsed using the **IntsegParser** method, following which the parsed documents go through the **IntsegAnalyzer** to get tokenized while **DirectoryIndexer** is trying to index the documents. At this point, after indexing is completed by Lucene (which requires some minutes), the **ISearcher** method searches through the indexed collection to retrieve a ranking of documents for each of the 50 evaluation topics (queries) available. The main function in charge of doing all above-mentioned steps is the **IntegSearchEngine.java** inside the code folder.

## How to run and use the codes? ##

* Before any attempt, make sure you have the **corpora** and **topics** files available in your system. 
* Also you need to have Project Debater Early Access Program credentials to be able to use IBM Project Debater in the searcher. In this case you can simply write an email to project.debater@il.ibm.com and kindly ask them to provide you with an **API Key**.

#### Running using CLI: ####
We provide here in the folder final_jar_executable a jar executable version of our program
that automatically creates its own working environment. It allows to set parameters for
our system by means of xml files. To run it follow these steps:
    
* Open the command line and change directory to where the INTSEG_SearchEngine-1.0-jar-with-dependencies.jar is.
* Run the following command passing the correct parameters:
```
java -cp .\INTSEG_SearchEngine-1.0-jar-with-dependencies.jar it.unipd.dei.se.IntsegSearchEngine "Path_To_Collection" "Path_To_Topics" "Path_To_Indexing_Directory"
```
#### Running using IntelliJ: ####

* Almost all the project dependencies are included in the pom.xml and the only jar dependency that needs to be added manually is under code/main/resources/java_api_project_debater.jar:
    * In IntelliJ go to File/Project Structure/Dependencies/Add/JARs or Directories... and select the provided .jar file.
    * Also make sure that the resources folder is marked as resources root. To do so, right-click on the resources folder, and choose Mark Directory as/Resources Root.
* Inside the intelliJ environemnt go through Run/Debug Configuration window. In the Program arguments field, pass the values as the following: 
    * "Path_To_Collection" "Path_To_Topics" "Path_To_Indexing_Directory" all separated by a whitespace.
* Run the whole project using the code/main/java/it.unipd.dei.se/IntsegSearchEngine.java method.

#### Note that: ####
On the first attempt of running the program, the working environment is going to be created and you need to provide an API Key. To do so, you can head to the project folder where you can find a new folder created named "config" which consists of "AnalyzerParams.xml" and "SearcherParams.xml". Open the SearcherParams.xml and simply put your private API Key inside the <ibm_api_key></ibm_api_key> tag.

Moreover, before starting your actual run you can play with Analyzer and Searcher parameters as you wish to configure your desired setting of the run.

After this step you are ready to rerun the program again using one of the above methods. A runtime might vary from 2 hours to 6 hours (in some cases) based on which parameters you have set in the config.

### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

