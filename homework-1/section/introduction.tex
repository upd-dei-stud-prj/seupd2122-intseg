\section{Introduction}
\label{sec:introduction}

Nowadays Search Engines are used by the vast majority of people in their daily routine to retrieve any kind of information in a practical and efficient way. Even though there exists several advanced implementations of them, there are still many big challenges such as the argument retrieval for controversial or comparative questions.


This report aims at providing an information retrieval system that has been developed for the Search Engines 2021/2022 course at the University of Padua concerning the task "Argument Retrieval for Controversial Questions" provided by the third Touché lab on argument retrieval at CLEF 2022. The ultimate goal of this system is to retrieve a pair of coherent arguments through a huge data-set of online debate portals in response to a query about a controversial topic.


The paper is organized as follows: Section~\ref{sec:related} describes the previous studies that have been done in this area; Section~\ref{sec:methodology} describes our approach; Section~\ref{sec:setup} explains our experimental setup; Section~\ref{sec:results} discusses our main findings; finally, Section~\ref{sec:conclusion} draws some conclusions and outlooks for future works.