\section{Methodology}
\label{sec:methodology}

In our system, the source file includes a parser, indexer, analyzer and a searcher in order to have a robust project structure. The general structure of the system can be divided into several parts. In particular each component has different aims as follows:
\begin{enumerate}
	\item \textbf{Parser}: It parses the corpus CSV file and extracts documents.
	\item \textbf{Analyzer}: It processes the extracted documents to apply tokenizers and stemmers.
	\item \textbf{Indexer}: It creates indexable fields based on the parsed documents.
	\item \textbf{Searcher}: It tries to retrieve relevant documents for each provided topic as a query.
\end{enumerate}

The details of each part are separately reported in sections \ref{ssec:Parser}, \ref{ssec:Analyzer}, \ref{ssec:Indexer}, \ref{ssec:Searcher}.

\subsection{Our System in General}
Our information retrieval system starts by parsing the collection obtaining the parsed documents. Each parsed document is analyzed and indexed keeping only the necessary information. Documents are indexed in 2 different folders:
\begin{itemize}
		\item \textbf{First Folder}: We store for each document its \emph{context} field and its \emph{sentences} field after having extracted only their \emph{ids}.
		\item \textbf{Second Folder}: We store each sentence found in the \emph{sentences} field with additional data such as its stance retrieved from the \emph{conclusion} and \emph{premises} fields.
\end{itemize}

% \begin{figure}[htbp!]
% 	\centering
% 	\includegraphics[width=14cm]{figure/indexing_diagram.png}
% 	\caption{Schema of our system representing the parsing, analyzing and indexing parts}
% \end{figure}

\begin{center}
	\captionsetup{labelfont=bf}
	\includegraphics[width=14cm]{figure/indexing_diagram.png}
	\captionof{figure}{Schema of our system representing the parsing, analyzing and indexing parts}
\end{center}

\noindent Topics are then parsed to formulate 2 queries for each of them: The first query will be used to retrieve the relevant sentences' \emph{ids} according only to the corresponding contexts while the second query will be used to retrieve relevant \emph{sentences}. Query expansion can be applied to formulate queries also by means of synonyms.

% \begin{figure}[htbp!]
% 	\centering
% 	\includegraphics[width=14cm]{figure/searching_diagram.png}
% 	\caption{Schema of our system representing the searching part}
% \end{figure}

\begin{center}
	\captionsetup{labelfont=bf}
	\includegraphics[width=14cm]{figure/searching_diagram.png}
	\captionof{figure}{Schema of our system representing the searching part}
\end{center}

%\begin{enumerate}
%	\item Starts by parsing the collection obtaining the parsed documents.
%	\item Each parsed document is analyzed and indexed keeping only the necessary information. Documents are indexed in 2 different folders:
%	\begin{itemize}
%		\item In one folder we store for each document its \emph{context} field and its \emph{sentences} field after having extracted only their ids.
%		\item In the other one we store each sentence found in the \emph{sentences} field with additional data such as its stance retrieved from the \emph{conclusion} and \emph{premises} fields.
%	\end{itemize}
%	%\begin{figure}[htbp!]
%	%	\centering
%	%	\includegraphics[width=14cm]{figure/indexing_diagram.png}
%	%	\caption{Schema of our system representing the parsing, analyzing and indexing parts}
%	%\end{figure}
%	\begin{center}
%		\captionsetup{labelfont=bf}
%		\includegraphics[width=14cm]{figure/indexing_diagram.png}
%		\captionof{figure}{Schema of our system representing parsing, analyzing and indexing}
%	\end{center}
%	
%	
%	\item Topics are then parsed and used to formulate queries. For each topic we formulate 2 queries: 
%	\begin{itemize}
%		\item The first query will be used to retrieve the relevant sentences' IDs according only to the corresponding contexts.
%		\item The second query will be used to retrieve relevant sentences. Query expansion can be applied to formulate queries also by means of synonyms.
%	\end{itemize}
%	\item A first re-ranking is done to establish which sentences are really relevant based both on their value but also on their context. After this operation we have 2000 sentences that were ranked as the most effective sentences.
%	\item Finally stances are retrieved for each sentence and another re-ranking is done to obtain the final ranking of both pro and con sentences to the given topic. In this method, sentences are paired based on their stance and returned as output ranked from the most relevant to the least relevant.
%	%\begin{figure}[htbp!]
%	%	\centering
%	%	\includegraphics[width=14cm]{figure/searching_diagram.png}
%	%	\caption{Schema of our system representing the searching part}
%	%\end{figure}
%	\begin{center}
%		\captionsetup{labelfont=bf}
%		\includegraphics[width=14cm]{figure/searching_diagram.png}
%		\captionof{figure}{Schema of our system representing the searching}
%	\end{center}
%\end{enumerate}

\subsection{Parser}
\label{ssec:Parser}
Our utmost goal at this step was to extract as much data as possible and not to lose any meaningful data. To achieve the aimed completeness, our parsed document consists of 5 fields which are document id, hash-table of premise ids and their texts, premise stances, hash-table of conclusion ids and their texts and context.

%The input data was in CSV unlike the previous year which was JSON. As a starting point we used the OpenCSV built-in library available in Java for reading and parsing the input. However, after some runs we modified the parsing method using JacksonCsvMapper because we achieved much less run-time as it is seen, for example, in the following table \cite{CSVParsers}:
%\newline 

%\begin{table}[ht]
%	\centering
%	\begin{tabular}{ |p{4.5cm}||p{3cm}|p{3.5cm}|  }
%		\hline
%		\textbf{Parser} & \textbf{Average Time} &\textbf{\% Slower than the Best}\\
%		\hline
%		uniVocity CSV Parser & 739 ms & 0\%\\
%		SimpleFlatMapper CSV Parser & 861 ms & 16\%\\
%		Jackson CSV Parser & 1212 ms & 64\%\\
%		...& ...&...\\
%		OpenCSV & 2022 ms & 173\%\\
%		\hline
%	\end{tabular}
%	\caption{CSV Parsers Runtime}
%\end{table}

Document ids were easy to extract while premise stances and hash-tables needed more effort. Our first approach to extract premise stances was done by means of regular expressions to find patterns of 'stance': 'PRO' or 'stance': 'CON' in the \emph{premises} column of the corpus. Moreover, the same was applied to extract premises/conclusions along with their ids. Here we have used regular expressions that iterates the \emph{sentences} column of the corpus and tries to find patterns of 'sent-id' and 'sent-text' as the starting points and then extracts only the clean id and text of each premise/conclusion. However, in the second approach we decided to simplify the use of regular expressions by combining them with Jackson JSON parser \cite{Jackson}. Finally, we kept the context part for later experiments to check whether a retrieved sentence is ideally the most representative sentence of its corresponding argument or not. Keeping contexts was very tricky because it affects the indexing runtime. %The following figure represents a parsed document as an example:

%\begin{center}
%	\captionsetup{labelfont=bf}
%	\includegraphics[width=14cm]{figure/parsed_doc.png}
%	\captionof{figure}{Example of fields of a parsed document}
%\end{center}


Through our experiments we noticed that in almost 10 percent of the whole corpus, there exists a conclusion in the \emph{conclusion} column but it does not appear in the \emph{sentences} column. This happens because some conclusion texts are less than 3 words and do not actually contain any stance information. For all these cases we have considered the conclusion as empty in the hash-table.


\subsection{Analyzer}
\label{ssec:Analyzer}
We implemented a custom analyzer which extends the abstract class Analyzer provided in Apache Lucene \cite{ApacheLucene}. We made the architecture flexible by parametrizing our analyzer which allows us to call it with different types of filters, tokenizers and stemmers. In this way the analyzer can be modified from the top layer without changing the core code each time. By changing the settings of these parameters it is possible to achieve better results in terms of precision. In addition, it is possible to increase or decrease both the index size and the indexing time required. We allow to choose from the possible parameters:

\begin{itemize}
	\item \textbf{Tokenizer}: Allows choosing from different tokenizers to use such as Whitespace, Letter and Standard.
	\item \textbf{Stemmer}: Allows choosing from different stemmers to use such as none, EnglishMinimal, Porter and Krovetz.
	\item \textbf{Token Length}: Allows specifying the minimum length for a token and its maximum length.
	\item \textbf{Stop-List}: Allows specifying a possible stop-list if used.
	\item \textbf{English Possessive}: Allows specifying if to apply or not the English Possessive Filter.
	\item \textbf{Shingles}: Allows specifying the size of the word n-grams if used. 
	\item \textbf{N-grams Size}: Allows specifying the size of n-grams if used.
\end{itemize}

%It is also possible to customize our Analyzer by means of the file AnalyzerParams.xml which looks as follows:\newline

%\begin{center}
%	\captionsetup{labelfont=bf}
%	\includegraphics[width=14cm]{figure/analyzer_params.png}
%	\captionof{figure}{An insight to the \emph{AnalyzerParams.xml} file}
%\end{center}

\subsubsection{Custom Stop-List}
We decided to analyze the indexed collection and try to create custom stop-lists composed of the most frequent terms in the indexed collection. Thus we parsed, analyzed without any kind of stemming and indexed the collection to inspect it with Luke \cite{Luke}.
Luke is a tool that can be used to inspect the index and obtain some statistics of the indexed files. After having analyzed the entire indexed collection with Luke, we managed to create 2 different stop-lists composed of 100 terms each: one that can be used for the \emph{context} field of the collection and one for the \emph{sentences} field. 

\subsection{Indexer}
\label{ssec:Indexer}
The arguments of the collection are stored in the index using four fields provided by the parser. 
% The implementation of our indexer part is done in a package named "indexer" which is based on five classes:
%\begin{enumerate}
%	\item \textbf{DirectoryIndexer}:\newline 
%	It uses the IntsegParser class to extract documents from the corpus file and builds the index into the indexer folder. When the main method is started, it calls the DirectoryIndexer's index method. After converting the documents, we focused on the development of how the indexer is created and used both for the indexing and searching phase.\newline 
%	%In IntsegAnalyzer, the tokenization starts with a StandardTokenizer and then reducing every word to lower case by using a LowerCaseFilter. Next it applies the minLength, maxLength, EnglishPossessiveFilter that removes the "'s" particle from the remaining tokens, following which, it uses the stop-words filter, StopFilterListName, that removes the words which are frequent in the whole collection which do not bring useful information and lastly EnglishMinimal. In the DirectoryIndexer, we have used LMDirichletSimilarity to represent the similarity and the ramBuffer to use as the buffered amount of documents.
%	We decided to index documents' \emph{context} and \emph{sentences} separately to decrease the index size and the indexing time. In one directory, passed as parameter, we store only one context for each document and the corresponding sentences' ids. In another directory, always passed as a parameter, we store each sentence id individually with its corresponding value and stance. Therefore it will be possible for the searcher to retrieve relevant contexts and sentences separately and match them in a second step.
%	\item \textbf{ContextField}:\newline 
%	Here, we create a custom Lucene field as a container for the context of a document. It is a tokenized field for keeping only the contexts. The method "IndexOptions.DOCS" is used to index only the documents, not term frequencies and positions.
%	\item \textbf{SentenceIDfield}:\newline
%	Here, we again create a custom Lucene field for the document sentence id. It basically creates a new field for the sentence id which can be both premise or conclusion id and stores their value.
%	\item \textbf{SentenceValueField}:\newline 
%	Here, there is another custom Lucene field for the document sentence value which stores the body of the premises and conclusions. \newline DOCS\_AND\_FREQS\_AND\_POSITIONS\_AND\_OFFSETS is used to index the documents, term frequencies, their positions and offsets.
%	\item \textbf{StanceField}:\newline 
%	The last custom Lucene field for the premise stances which can be PRO or CON.
%\end{enumerate}
It is possible to avoid indexing some sentences that are recognized as spam. By looking at the document collection, in fact, we found many sentences that contained random characters or words in other languages. Therefore we decided to apply a language model provided by Apache Open NLP \cite{ApacheOpenNLP} to retrieve the language of a sentence: if a sentence is not recognized as being written in English, the sentence is discarded. This resulted in having roughly 300 thousands sentences removed from the indexed documents but this requires additional time when indexing. It is possible to see the details obtained from some of our runs regarding the index size and indexing time based on the parameters used in the analyzer and based on the utilization of the spam method used in the following table:

\begin{center}
    \captionsetup{labelfont=bf}
    \begin{table}[ht]
    	\centering
    	\begin{tabular}{ |p{1.8cm}|p{1.4cm}|p{1.5cm}|p{1.5cm}|p{1.1cm}|p{1.4cm}||p{0.9cm}|p{1.2cm}|  }
    		%\hline
    		%\multicolumn{8}{|c|}{Indexing with Different Settings for the Analyzer's Parameters} \\
    		\hline
    		\textbf{Tokenizer}&\textbf{Stemmer} &\textbf{Minimum \newline token length}&\textbf{Maximum token length}&\textbf{Stoplist}&\textbf{Spam \newline detection}&\textbf{Time}&\textbf{Total size}\\
    		\hline
    		WhiteSpace&Krovetz &3&10&Yes&No&527 s&1.01 GB\\
    		&&&&&&&\\
    		WhiteSpace&Porter &2&10&Yes&No&559 s&1.00 GB\\
    		&&&&&&&\\
    		Standard&English Minimal &2&20&No&No&451 s&1.10 GB\\
    		&&&&&&&\\
    		WhiteSpace&English Minimal &2&20&No&Yes&1057 s&1.06 GB\\
    		\hline
    	\end{tabular}
    \end{table}
    \captionof{table}{Indexing with different settings for the analyzer's parameters}
\end{center}

\subsection{Searcher}
\label{ssec:Searcher}
The searcher is the component that is in charge of retrieving, from a given topic, a ranked list of documents from the most to the least relevant. In this case the searcher aims at retrieving the best 1000 pairs of sentences for a given topic. Each couple of sentences should have the same stance that can be either \emph{PRO} in case the pair of sentences is supporting the topic or \emph{CON} vice versa. Our developed searcher integrates different tools:
\begin{itemize}
	\item \textbf{Query Expansion}: This is a technique that allows us to reformulate a query in order to increase the number of matched documents.
	\item \textbf{Term Weighting}: This is a technique consisting of assigning different weights to each term of the query based on its discriminant power on the collection.
	\item \textbf{Document Re-Ranking}: This is a technique that is applied after having retrieved a large set of relevant documents. It involves applying different operations to rank the set of retrieved document once again and keep only a smaller subset composed by the most relevant ones.
\end{itemize}

%It is possible to customize the ISearcher class by directly passing parameters or by modifying and passing the file \textbf{SearcherParams.xml} which provides an easy way to customize most of our IR system parameters.

%\begin{center}
%	\captionsetup{labelfont=bf}
%	\includegraphics[width=14cm]{figure/searcher_params.png}
%	\captionof{figure}{A view to the file \emph{SearcherParams.xml}}
%\end{center}

We added the possibility to formulate queries based on both topics' title and description. After running our system we found that formulating queries using also the topics' description was deteriorating the precision of our system, therefore we decided to consider only the topics' title and to not further implement the retrieval based also on the description field. The searcher first parses the provided topics to extract their fields and then formulates 2 queries:
\begin{enumerate}
	\item \textbf{First Query}: It is used to retrieve relevant sentences' id that can be either premises or conclusions based only on the \emph{context} field of the collection.
	\item \textbf{Second Query}: It is used to retrieve relevant sentences that can be either premises or conclusions based only on the \emph{sentences} field of the collection.
\end{enumerate}
After obtaining the 2 lists of relevant documents corresponding to each query, we only keep the best 2000 sentences based on the results of the 2 queries. This is shown in the \emph{Algorithm 1}. 

\begin{algorithm}
	\caption{Filter ranked documents}\label{euclid}
	\begin{algorithmic}[1]
		\Procedure{getBest2000RankedSentences}{}
		\State $\textit{listSentencesFirstQuery} \gets \textit{firstQuery.execute() // sentences from collection\'s contexts field}$
		\State $\textit{listSentencesSecondQuery} \gets \textit{secondQuery.execute() // sentences from collection\'s sentences field}$
		\State $\textit{P1} \gets \text{weight parameter for contexts} $
		\State $\textit{P2} \gets \text{weight parameter for sentences} $
		\State $\textit{finalListSentences} \gets \text{empty}$
		\newline
		\State $\forall \textit{ sentence} \in \textit{listSentencesSecondQuery} :$
		\State \textbf{\quad if} $\textit{sentence} \in \textit{listSentencesFirstQuery} :$
		\State \quad \quad $\textit{sentence.weight=} \textit{sentence.weight*}\textit{P2} +\textit{listSentencesFirstQuery.get(sentence).weight*}\textit{P1}$
		\State \quad $\textit{finalListSentences.add(} \textit{sentence)}$
		\newline
		%\State $\forall \textit{ sentence} \in \textit{listSentencesFirstQuery} :$
		%\State \textbf{\quad if} $\textit{sentence} \not \in \textit{finalListSentences} :$
		%\State \quad \quad $\textit{sentence.weight=}\textit{sentence.weight*}\textit{P1}$
		%\State \quad \quad $\textit{finalListSentences.add(} \textit{sentence)}$
		%\newline
		\State $\textit{finalListSentences.orderByDescendingWeight()} $
		\State $\textit{finalListSentences=finalListSentences.}\textit{keepOnlyFirst2000()} $
		\State $ \textbf{return} \textit{ finalListSentences}$
		%\State $i \gets \textit{patlen}$
		%\BState \emph{top}:
		%\If {$i > \textit{stringlen}$} \Return false
		%\EndIf
		%\State $j \gets \textit{patlen}$
		%\BState \emph{loop}:
		%\If {$\textit{string}(i) = \textit{path}(j)$}
		%\State $j \gets j-1$.
		%\State $i \gets i-1$.
		%\State \textbf{goto} \emph{loop}.
		%\State \textbf{close};
		%\EndIf
		%\State $i \gets i+\max(\textit{delta}_1(\textit{string}(i)),\textit{delta}_2(j))$.
		%\State \textbf{goto} \emph{top}.
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\subsubsection{Query Expansion}
We tried to implement the query expansion part by using the WordNet \cite{Wordnet} thesaurus which provides a list of words and their corresponding synonyms.
Each topic is processed in a way that when applying query expansion, we try to retrieve the possible synonyms for each term in the topic and consider them when formulating the final query.

Since each term can have different grammatical meaning based on the context (e.g. can be either a noun or a verb) and consequently the list of synonyms can be different, we first use the Apache Open NLP \cite{ApacheOpenNLP} library to mark up each term as corresponding to a particular part of speech (POS). We tested the POS-Tagger with 2 different models provided by Apache Open NLP that are \textbf{en-pos-maxent.bin} and \textbf{en-pos-perceptron.bin} and we eventually decided to use only the first one because it was generally more accurate.

After having tagged each term, we retrieve from the WordNet dataset the list of synonyms associated with each term based on its tag. This is achieved by keeping an in-memory representation of the dataset through a HashMap. Thus we can obtain search operations in \emph{O(1)}.

The final query is then formulated by means of the Lucene \cite{ApacheLucene} query classes \textbf{BooleanQuery}, \textbf{BoostQuery}, \textbf{TermQuery} and \textbf{PhraseQuery} in the following way:
\begin{enumerate}
	\item Each term of the topic title considering both the original terms of the topic and synonyms is wrapped in a \textbf{TermQuery} object. If a synonym is composed of many terms (e.g. one of the synonyms for the term \emph{'search'} is \emph{'look for'} ), then we use a \textbf{PhraseQuery} instead of a \textbf{TermQuery} when creating the second query (the one used to retrieve sentences based only on the topics' sentences field). In fact, we index sentences storing offset values as well, and this allows \textbf{PhraseQuery} for multiple terms matching.
	\item Each \textbf{TermQuery}/\textbf{PhraseQuery} object is then wrapped in a \textbf{BoostQuery} object that allows to specify a weight. These parameters can be set in our ISearcher class as parameters that depend on the POS tag of the term and whether it is a synonym or an original term of the topic.
	\item Each \textbf{BoostQuery} object is then added to a final \textbf{BooleanQuery} that represents the \emph{OR} of all the added BoostQueries.
\end{enumerate}

\subsubsection{Term Weighting}
When formulating a query, term weighting can be incredibly useful in emphasizing the importance of some terms over others. In fact, after having inspected the collection, we found that many terms appeared in nearly every document while others were found in only some of them. It is possible to see that terms appearing in almost every document do not have a high discriminant power: a query based only on that term would retrieve almost every document in the collection. We implemented our Searcher with 2 possible Term Weighting measures:
\begin{itemize}
	\item \textbf{TF-IDF}: Over the whole collection. It is a customized version of the normal TF-IDF in which the term frequency TF is calculated over all the terms in the collection and not for the terms in the considered query. We also tried to smooth it by means of the fifth root.
	Let $tot_{tk}$ be the total number of tokens in the collection, $tot_{doc}$ be the total number of documents in the collection, $|token_{i}|$ be the number of occurrences of the $i$-th considered token over the whole collection and  $|doc_{token_i}|$ be the number of documents containing at least one occurrence of the $i$-th considered token. Then the weight of a term according to this measure can be calculated as follows: 
	
	\begin{center}
		$TFIDF(token_{i}) = TF(token_{i}) \cdot IDF(token_{i})  =   \sqrt[5]{\frac{|token_{i}|} {tot_{tk}}} \cdot (\log \frac{tot_{doc}} {|doc_{token_i}|} )$
	\end{center}
	
	\item \textbf{I-Coefficient}: This is a coefficient that we came up with to calculate the discriminant power of a term. Let $tot_{doc}$ be the total number of documents in the collection, $|token_{i}|$ be the number of occurrences of the $i$-th considered token over the whole collection and  $|doc_{token_i}|$ be the number of documents containing at least one occurrence of the $i$-th considered token. Then the weight of a term according to this measure can be calculated as follows:
	
	\begin{center}
		$I_{coef}(token_{i}) =  (1-\frac{|doc_{token_i}|} {tot_{doc}} )\cdot (1- \frac{|doc_{token_i}|} {2\cdot|token_{i}|} )$
	\end{center}
	
\end{itemize}

\subsubsection{Document Re-Ranking}
We implemented document re-ranking using IBM Project Debater. This is an AI system developed by IBM that was developed to debate humans on complex topics \cite{IBMProjectDebater}. It has been in development since 2012 and it is based on a massive dataset. To use project debater it is necessary to have an internet connection since each API call is done through http requests, then IBM servers elaborate the corresponding answer and send back an http response containing it. Using project debater therefore requires almost no computation by the computer that is running our system, but it takes quite some time to obtain an answer for all the topics.

We used the project debater Java library to exploit the following 2 APIs:

\begin{itemize}
	\item \textbf{Pro/Cons API}: This API is used to understand if a sentence most strongly opposes the topic or most strongly supports it. This is used to obtain which of the 2000 retrieved sentences by our system are \emph{PRO} or \emph{CON} a given topic. In fact, the final output of our program needs to have a couple of sentences having same stance. \newline
	This API is used by default in our system; the time taken to obtain the result for 50 topics considering only this part of our system is in the order of 1 hour.
	\item \textbf{Evidence Detection API}: This API is used to determine if a sentence is likely to contain evidence relating to a topic and vice versa. By means of this API we saw a significant improvement in the results, especially considering the best ranked sentences. \newline
	Using this API in our system is optional; the time taken to obtain the result for 50 topics considering only this part of our system is in the order of 4-5 hours.
\end{itemize}