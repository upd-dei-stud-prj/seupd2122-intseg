package it.unipd.dei.se.tools;


import com.ibm.hrl.debater.clients.DebaterApi;
import com.ibm.hrl.debater.clients.SentenceTopicPair;
import com.ibm.hrl.debater.clients.evidence_detection.EvidenceDetectionClient;
import com.ibm.hrl.debater.clients.pro_con.ProConClient;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that implements some APIs provided by the IBM Project Debater that can
 * be found in the following <a href="https://research.ibm.com/interactive/project-debater/">link</a>.
 * In particular, it provides static methods implementing the Pro/Con API and the
 * Evidence Detection API.
 * To use Project Debater it's necessary to provide a valid API-Key that won't be
 * stored here publicly.
 *
 * @author Andrea Pasin
 */
public class DebaterManager {

    /**
     * Uses the IBM Project Debater Pro/Cons API to retrieve if the provided sentences are pro or con the query topic.
     * The results are generally better if question mark is removed from the query title!
     *
     * @param query  the query used to establish if sentences are pro or con
     * @param sentences  the list of sentences (premises or conclusions) to check if they are pro or cons the query topic.
     * @param apiKey  the API-Key to make use of the IBM Debater
     * @return  a list of float values in range [-1,1]. Each value represents how much the corresponding sentence matches
     * the query sentiment (e.g. if the value is -1 the sentence opposes the query topic otherwise if it is +1 the sentence
     * supports the query topic)
     * @throws IOException
     */
    public static List<Float> getProConScore(@NotNull String query, @NotNull List<String> sentences, @NotNull String apiKey) throws IOException{
        if(sentences==null||sentences.isEmpty()){
            return new ArrayList<Float>();
        }
        ProConClient proConClient = DebaterApi.builder().build().getProConClient();
        return proConClient.getScores(sentences.stream().map(sent-> SentenceTopicPair.builder().sentence(sent).topic(query).build())
                .collect(Collectors.toList()), apiKey);
    }

    /**
     * Uses the IBM Project Debater Evidence Detection API to retrieve if the provided sentences contain evidence relating to the query topic
     * The results are generally better if question mark is removed from the query title!
     *
     * @param query  the query used to establish if sentences contain evidence relating to itself
     * @param sentences  the list of sentences (premises or conclusions) to check if they contain evidence relating to the query.
     * @param apiKey  the API-Key to make use of the IBM Debater
     * @return  a list of float values in range [0,1]. Each value represents how much the corresponding sentence matches
     * the query topic without considering its sentiment (e.g. if the value is 0 the sentence unlikely contains evidence
     * relating to the query topic otherwise if it is +1 the sentence is likely to contain evidence relating to the query topic)
     * @throws IOException
     */
    public static List<Float> getEvidenceScores(@NotNull String query, @NotNull List<String> sentences, @NotNull String apiKey) throws IOException{
        if(sentences==null||sentences.isEmpty()){
            return new ArrayList<Float>();
        }
        List<SentenceTopicPair> sentenceTopicPairs =new ArrayList<>();
        for(String sentence:sentences){
            sentenceTopicPairs.add(new SentenceTopicPair(sentence,query));
        }
        EvidenceDetectionClient edc = DebaterApi.builder().build().getEvidenceDetectionClient();
        return edc.getScores(sentenceTopicPairs, apiKey);
    }

    /**
     * Just for testing purposes
     */
    public static void main(String[]args){
        List<String> sentences=new ArrayList<>();
        sentences.add("I hate cats");
        sentences.add("I love cats");
        sentences.add("Today is sunny");
        try{
            List<Float> scores=DebaterManager.getProConScore("Are cats beautiful",sentences,"INSERT HERE THE API KEY");
            for(float f:scores){
                System.out.println(f);
            }
        }
        catch (Exception ex){

        }
    }
}

