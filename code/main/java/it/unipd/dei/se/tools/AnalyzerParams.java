package it.unipd.dei.se.tools;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Class used to define the analyzer params based on xml
 *
 * @author Andrea Pasin
 */
public class AnalyzerParams {
    @JacksonXmlProperty
    private String tokenizer_type; //the key to use Project Debater
    @JacksonXmlProperty
    private int min_length;   //how many sentences should be retrieved when applying the query only on sentences.
    @JacksonXmlProperty
    private int max_length;   //how many sentences should be retrieved when applying the query only on sentences.
    @JacksonXmlProperty
    private int use_english_possessive_filter;    //set to 1 if this should be used in the Searcher otherwise set to 0.
    @JacksonXmlProperty
    private int use_stop_lists;    //set to 1 if this should be used in the Searcher otherwise set to 0.
    @JacksonXmlProperty
    private String stem_filter;    //set to 1 if this should be used in the Searcher otherwise set to 0. This and TFIDF_QUERY_WEIGHTING are mutually exclusive!
    @JacksonXmlProperty
    private Integer n_gram_filter_size;    //set to 1 if this should be used in the Searcher otherwise set to 0. This and ICOEFFICIENT_QUERY_WEIGHTING are mutually exclusive!
    @JacksonXmlProperty
    private Integer shingle_filter_size;    //set to 1 if this should be used in the Searcher otherwise set to 0.

    public String getTokenizer_type() {
        return tokenizer_type;
    }

    public void setTokenizer_type(String tokenizer_type) {
        this.tokenizer_type = tokenizer_type;
    }

    public int getMin_length() {
        return min_length;
    }

    public void setMin_length(int min_length) {
        this.min_length = min_length;
    }

    public int getMax_length() {
        return max_length;
    }

    public void setMax_length(int max_length) {
        this.max_length = max_length;
    }

    public int getUse_english_possessive_filter() {
        return use_english_possessive_filter;
    }

    public void setUse_english_possessive_filter(int use_english_possessive_filter) {
        this.use_english_possessive_filter = use_english_possessive_filter;
    }

    public int getUse_stop_lists() {
        return use_stop_lists;
    }

    public void setUse_stop_lists(int use_stop_lists) {
        this.use_stop_lists = use_stop_lists;
    }

    public String getStem_filter() {
        return stem_filter;
    }

    public void setStem_filter(String stem_filter) {
        this.stem_filter = stem_filter;
    }

    public Integer getN_gram_filter_size() {
        return n_gram_filter_size;
    }

    public void setN_gram_filter_size(Integer n_gram_filter_size) {
        this.n_gram_filter_size = n_gram_filter_size;
    }

    public Integer getShingle_filter_size() {
        return shingle_filter_size;
    }

    public void setShingle_filter_size(Integer shingle_filter_size) {
        this.shingle_filter_size = shingle_filter_size;
    }
}
