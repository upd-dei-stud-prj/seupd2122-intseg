package it.unipd.dei.se.tools;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import it.unipd.dei.se.searcher.TokenWeightTag;
import opennlp.tools.langdetect.Language;
import opennlp.tools.langdetect.LanguageDetector;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.LanguageDetectorModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A class for POS tagging and language detection using ApacheOpenNLP.
 *
 * @author Andrea Pasin
 */
public class ApacheNLPManager {
    private static POSTaggerME posTagger;
    private static LanguageDetector languageDetector;
    private static boolean initialized=false;

    /*public enum TAG {
        CD("Cardinal number"),
        DT("Determiner"),
        EX("Existential there"),
        FW	("Foreign word"),
        IN	("Preposition or subordinating conjunction"),
        JJ	("Adjective"),
        JJR	("Adjective, comparative"),
        JJS	("Adjective, superlative"),
        LS	("List item marker"),
        MD	("Modal"),
        NN	("Noun, singular or mass"),
        NNS("Noun, plural"),
        NNP("Proper noun, singular"),
        NNPS("Proper noun, plural"),
        PDT("Predeterminer"),
        POS("Possessive ending"),
        PRP("Personal pronoun"),
        PRP$("Possessive pronoun"),
        RB("Adverb"),
        RBR("Adverb, comparative"),
        RBS("Adverb, superlative"),
        RP("Particle"),
        SYM("Symbol"),
        TO("to"),
        UH("Interjection"),
        VB("Verb, base form"),
        VBD("Verb, past tense"),
        VBG("Verb, gerund or present participle"),
        VBN("Verb, past participle"),
        VBP("Verb, non-3rd person singular present"),
        VBZ("Verb, 3rd person singular present"),
        WDT("Wh-determiner"),
        WP("Wh-pronoun"),
        WP$("Possessive wh-pronoun"),
        WRB("Wh-adverb");

        private final String description;
        TAG(final String description) {
            this.description=description;
        }

        @Override
        public String toString() {
            return description;
        }
    }*/

    /**
     * Initializes the ApacheNLP. If already initialized it does not do  anything
     *
     * @param posModelPath  the path to the POS model
     * @param languageModelPath the path to the language model
     * @throws IOException
     */
    public synchronized static void init(@NotNull final ResourcesUtil.ResourcePath posModelPath,@NotNull ResourcesUtil.ResourcePath languageModelPath)throws IOException{
        if(initialized){
            return;
        }
        posTagger=new POSTaggerME(new POSModel(ResourcesUtil.getFileInputStream(posModelPath)));
        languageDetector=new LanguageDetectorME(new LanguageDetectorModel(ResourcesUtil.getFileInputStream(languageModelPath)));
        initialized=true;
    }

    /**
     * Initializes the ApacheNLP again with different parameters
     *
     * @param posModelPath  the path to the POS model
     * @param languageModelPath the path to the language model
     * @throws IOException
     */
    public synchronized static void reInit(@NotNull final String posModelPath,@NotNull String languageModelPath)throws IOException{
        posTagger=new POSTaggerME(new POSModel(new FileInputStream(posModelPath)));
        languageDetector=new LanguageDetectorME(new LanguageDetectorModel(new File(languageModelPath)));
        initialized=true;
    }

    /**
     * Gets if initialized
     *
     * @return  true if initialized
     */
    public static boolean isInitialized(){
        return initialized;
    }

    /**
     * Gets a list of {@link TokenWeightTag} where each token will be tagged according to ApacheOpenNLP POS Tagger
     *
     * @param tokens  the query divided into tokens
     * @return  a list of {@link TokenWeightTag} where each token is tagged
     * @throws IOException
     * @throws IllegalStateException  if not initialized
     */
    public static List<TokenWeightTag> queryPOSTag(@NotNull final String[] tokens, @NotNull WeightParams weightParams) throws IOException {
        if(!initialized){
            throw new IllegalStateException("Not initialized");
        }
        String[] tags = posTagger.tag(tokens);
        double probs[] = posTagger.probs();
        List<TokenWeightTag> tokenTagProbabilities=new ArrayList<>();
        for(int i=0;i<tags.length;i++){
            if(isNoun(tags[i])){
                tokenTagProbabilities.add(new TokenWeightTag(tokens[i],weightParams.getNoun(),tags[i],probs[i]));
            }
            else {
                tokenTagProbabilities.add(new TokenWeightTag(tokens[i],weightParams.getNot_noun(),tags[i],probs[i]));
            }

        }
        return tokenTagProbabilities;
    }

    /**
     * Detects if the language is English for the given sentence according to ApacheOpenNLP POS Tagger
     *
     * @param sentence  the sentence for which we want to get the language
     * @return  true if the sentence is written in English
     * @throws IOException
     * @throws IllegalStateException  if not initialized
     */
    public static boolean isEnglish(@NotNull final String sentence) throws IOException {
        if(!initialized){
            throw new IllegalStateException("Not initialized");
        }
        Language[] languages=languageDetector.predictLanguages(sentence);
        return languages[0].getLang().equals("eng");
    }

    /**
     * Returns the corresponding {@link WordNetManager.TAG} from a given ApacheOpenNLP tag.
     * Follow this <a href="https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html">link</a> to
     * see all possible ApacheOpenNLP tags.
     *
     * @param tag  the ApacheOpenNLP tag
     * @return  null if the provided tag does not belong to this list: JJ, JJR, JJS, NN, NNS, NNP, NNPS,
     * RP, RBR, RBS, VB, VBD, VBG, VNB, VBP, VBZ otherwhise it returns the corrisponding tag to be used in WordNet
     */
    public static WordNetManager.TAG fromApacheToWordnet(@NotNull final String tag){
        switch (tag) {
            case "JJ","JJR", "JJS" -> {return WordNetManager.TAG.ADJECTIVE;}
            case "NN","NNS","NNP","NNPS" -> {return WordNetManager.TAG.NOUN;}
            case "RB","RBR","RBS" -> {return WordNetManager.TAG.ADVERB;}
            case "VB","VBD","VBG","VNB","VBP","VBZ" -> {return WordNetManager.TAG.VERB;}
            default -> {return null;}
        }
    }

    private static boolean isNoun(@NotNull final String tag){
        if(tag.equals("NN")||tag.equals("NNS")||tag.equals("NNP")||tag.equals("NNPS")){
            return true;
        }
        return false;
    }

    private static boolean isVerb(@NotNull final String tag){
        if(tag.equals("VB")||tag.equals("VBD")||tag.equals("VBG")||tag.equals("VNB")||tag.equals("VBP")||tag.equals("VBZ")){
            return true;
        }
        return false;
    }

}
