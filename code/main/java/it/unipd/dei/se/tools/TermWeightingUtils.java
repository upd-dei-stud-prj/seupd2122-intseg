package it.unipd.dei.se.tools;
import it.unipd.dei.se.analyzer.IntsegAnalyzer;
import it.unipd.dei.se.parser.IntsegParser;
import it.unipd.dei.se.parser.ParsedDocument;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.util.*;

/**
 * Class keeping in memory a HashMap of all the tokens with statistics.
 * The amount of RAM required is in the order of ~100 MB after having analyzed the entire corpus and
 * applied stemmer considering only the context field.
 * This can be used to extrapolate which tokens in a query are the most discriminant ones and
 * can be used to privilege the retrieval of some documents over others.<br>
 *
 * @author Andrea Pasin
 */
public class TermWeightingUtils {
    private final HashMap<String, TokenStats> tokensStats;
    //private static final int totDocuments=365408;
    private final int totalNumberOfTokens;
    private final int totDocuments;

    /*public TermWeightingUtils(@NotNull final List<TokenStats> tokensWithStats, final int totalNumberOfTokens){
        tokensStats=new HashMap<String,TokenStats>();
        for(TokenStats tk:tokensWithStats){
            tokensStats.put(tk.getToken(),tk);
        }
        this.totalNumberOfTokens=totalNumberOfTokens;
    }*/

    public TermWeightingUtils(@NotNull final ResourcesUtil.ResourcePath path) throws IOException{
        tokensStats=new HashMap<String,TokenStats>();
        //this.totalNumberOfTokens=init(path);
        int[] values=init(path);
        this.totDocuments=values[0];
        this.totalNumberOfTokens=values[1];
    }

    /**
     * Initializes the HashMap with all the tokens and corrispective stats saved in an appropriate file
     * with format:
     * 'totalNumberOfDocuments'+'\n'
     * 'totalNumberOfTokens'+'\n'
     * 'word'+'\t'+'documentOccurrences'+'\t'+'totalOccurrences'+'\n'
     * ...
     *
     * @param datasetPath  the path to the file containing list of tokens with corresponding stats (e.g. /src/.../datasets/Icoefficient_dataset.txt)
     * @throws IOException  if the provided path is not valid
     */
    private int[] init(@NotNull ResourcesUtil.ResourcePath datasetPath) throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(ResourcesUtil.getFileInputStream(datasetPath)));
        System.out.println("Starting reading through the Dataset to initialize TermWeightingUtils...");
        String line;
        int totalNumberOfDocuments=0;
        int totalNumberOfWords=0;
        if((line=br.readLine()) != null){
            totalNumberOfDocuments=Integer.parseInt(line);
        }
        if((line=br.readLine()) != null){
            totalNumberOfWords=Integer.parseInt(line);
        }
        while ((line=br.readLine()) != null) {
            String[] fields=line.split("\t");
            tokensStats.put(fields[0],new TokenStats(fields[0],Integer.parseInt(fields[1]),Integer.parseInt(fields[2])));
        }
        return new int[]{totalNumberOfDocuments,totalNumberOfWords};
    }

    /**
     * Saves the HashMap with all the tokens and corrispective stats saved in an appropriate file specified
     * with format 'word'+'\t'+'documentOccurrences'+'\t'+'totalOccurrences'+'\n' for each word
     *
     * @param datasetPath  the path to the file to write to (e.g. /src/.../datasets/Icoefficient_dataset.txt)
     * @throws IOException  if the provided path is not valid
     */
    public void saveDataset(@NotNull String datasetPath) throws IOException {
        PrintWriter pw = new PrintWriter(new FileWriter(datasetPath));
        pw.println(totalNumberOfTokens);
        for (String key : tokensStats.keySet()) {
            pw.println(key+"\t"+tokensStats.get(key).totDocumentsAppeared+"\t"+tokensStats.get(key).tokenOccurrences);
        }
        pw.close();
    }

    /**
     * Calculates the INTSEG coefficient for a given token
     * The INTSEG coefficient is a coefficient that ranges in [0,1] and represents how discriminant
     * a token is in retrieving a particular document over the whole collection.
     * It is based on the fact that each token appearing in a small percentage of documents
     * but with a high mean value of occurrences over the documents in which it appears is more
     * discriminant than other tokens. In fact, if a query contains that specific term, it's likely
     * that we want to retrieve one of the few documents over the whole collection.
     *
     * @param token  the token to calculate the coefficient for
     * @return  the coefficient associated to the provided token
     */
    public final float calculateINTSEGcoef(@NotNull String token){
        TokenStats t=tokensStats.get(token);
        if(t==null){
            return 0f;
        }
        return (1-(float)t.getTotDocsAppeared()/totDocuments)*(1-(float)t.getTotDocsAppeared()/(2*t.getTokenOccurrences()));
    }

    /**
     * Calculates a customized version of the TFIDF weight for a given token.
     * The TFIDF is calculated using as TF the frequency of the given token
     * considering the whole collection and not just a document or the query.
     * In this case we try to obtain the frequency of term over the whole collection
     * and smooth it.
     *
     * @param token  the token to calculate the weight for
     * @return  the weight associated to the provided token
     */
    public final float calculateTFIDF(@NotNull String token){
        TokenStats t=tokensStats.get(token);
        if(t==null){
            return 0f;
        }
        float collectionTF=(float)Math.pow((float)t.getTokenOccurrences()/totalNumberOfTokens,0.2f);
        float collectionIDF=(float)Math.log10((float)totDocuments/(t.getTotDocsAppeared()));
        if(collectionIDF<0){
            collectionIDF=0;
        }
        return (collectionIDF*collectionTF);
    }

    /**
     * Reads the collection, extracts statistics about contexts and write an appropriate file called 'sentence_stats.txt' that
     * can be read to perform token weighting. The output file should be saved into the 'resources' folder in order
     * to be included in the jar package.
     *
     * @param a  the Analyzer to use. Notice that the Analyzer should be without stemmer or with EnglishMinimal stemmer.
     * @param pathToCsv  the path to the collection
     * @param pathToSave  the path to the save folder.
     * @throws IOException
     */
    public static void getContextInfoAndSave(Analyzer a, String pathToCsv, String pathToSave) throws IOException {
        HashMap<String, TokenStats> tokensStatsToAdd=new HashMap<>();
        int totTokens=0;
        int totDocs=0;
        IntsegParser parser=new IntsegParser(pathToCsv);
        System.out.println("START INITIALIZATION OF THE CONTEXT STATS");
        for(ParsedDocument pd:parser){
            totDocs++;
            String context=pd.get_context();
            TokenStream stream=a.tokenStream("field", new StringReader(context));
            final CharTermAttribute tokenTerm = stream.addAttribute(CharTermAttribute.class);
            stream.reset();
            HashMap<String,Integer> tokens=new HashMap<String, Integer>();
            while(stream.incrementToken()){
                totTokens++;
                if(tokens.containsKey(tokenTerm.toString())){
                    tokens.put(tokenTerm.toString(),tokens.get(tokenTerm.toString())+1);
                }
                else{
                    tokens.put(tokenTerm.toString(),1);
                }
            }
            for(String tk:tokens.keySet()){
                if(tokensStatsToAdd.containsKey(tk)){
                    tokensStatsToAdd.put(tk,new TokenStats(tk,tokensStatsToAdd.get(tk).totDocumentsAppeared+1,tokensStatsToAdd.get(tk).tokenOccurrences+tokens.get(tk)));
                }
                else{
                    tokensStatsToAdd.put(tk,new TokenStats(tk,1,tokens.get(tk)));
                }
            }
            if(totDocs%10000==0){
                System.out.println((totDocs/10000) +"0k documents evaluated");
            }
            stream.close();
        }
        System.out.println("FINISHED INITIALIZATION OF THE CONTEXT");
        System.out.println("START SAVING FILE");
        PrintWriter pw = new PrintWriter(new FileWriter(pathToSave));
        pw.println(totDocs);
        pw.println(totTokens);
        for (String key : tokensStatsToAdd.keySet()) {
            pw.println(key+"\t"+tokensStatsToAdd.get(key).totDocumentsAppeared+"\t"+tokensStatsToAdd.get(key).tokenOccurrences);
        }
        pw.close();
        System.out.println("FINISH SAVING FILE");
    }

    /**
     * Reads the collection, extracts statistics about sentences and write an appropriate file called 'sentence_stats.txt' that
     * can be read to perform token weighting. The output file should be saved into the 'resources' folder in order
     * to be included in the jar package.
     *
     * @param a  the Analyzer to use. Notice that the Analyzer should be without stemmer or with EnglishMinimal stemmer.
     * @param pathToCsv  the path to the collection
     * @param pathToSave  the path to the save folder.
     * @throws IOException
     */
    public static void getSentencesInfoAndSave(Analyzer a, String pathToCsv,String pathToSave) throws IOException {
        HashMap<String, TokenStats> tokensStatsToAdd=new HashMap<>();
        int totTokens=0;
        int totDocs=0;
        IntsegParser parser=new IntsegParser(pathToCsv);
        System.out.println("START INITIALIZATION OF SENTENCES STATS");
        for(ParsedDocument pd:parser){
            HashMap<String,String> premises=pd.get_prem_id_body();
            HashMap<String,String> conclusions=pd.get_conc_id_body();
            StringBuilder sentencesBuilder=new StringBuilder();
            for(String k:premises.keySet()){
                sentencesBuilder.append(premises.get(k)).append(" ");
                totDocs++;
            }
            for(String k:conclusions.keySet()){
                sentencesBuilder.append(conclusions.get(k)).append(" ");
                totDocs++;
            }
            TokenStream stream=a.tokenStream("field", new StringReader(sentencesBuilder.toString()));
            final CharTermAttribute tokenTerm = stream.addAttribute(CharTermAttribute.class);
            stream.reset();
            HashMap<String,Integer> tokens=new HashMap<String, Integer>();
            while(stream.incrementToken()){
                totTokens++;
                if(tokens.containsKey(tokenTerm.toString())){
                    tokens.put(tokenTerm.toString(),tokens.get(tokenTerm.toString())+1);
                }
                else{
                    tokens.put(tokenTerm.toString(),1);
                }
            }
            for(String tk:tokens.keySet()){
                if(tokensStatsToAdd.containsKey(tk)){
                    tokensStatsToAdd.put(tk,new TokenStats(tk,tokensStatsToAdd.get(tk).totDocumentsAppeared+1,tokensStatsToAdd.get(tk).tokenOccurrences+tokens.get(tk)));
                }
                else{
                    tokensStatsToAdd.put(tk,new TokenStats(tk,1,tokens.get(tk)));
                }
            }
            if(totDocs%10000==0){
                System.out.println((totDocs/10000) +"0k documents evaluated");
            }
            stream.close();
        }
        System.out.println("FINISH INITIALIZATION OF SENTENCES STATS");
        System.out.println("START SAVING FILE");
        PrintWriter pw = new PrintWriter(new FileWriter(pathToSave));
        pw.println(totDocs);
        pw.println(totTokens);
        for (String key : tokensStatsToAdd.keySet()) {
            pw.println(key+"\t"+tokensStatsToAdd.get(key).totDocumentsAppeared+"\t"+tokensStatsToAdd.get(key).tokenOccurrences);
        }
        pw.close();
        System.out.println("FINISH SAVING FILE");
    }



    /**
     * Class representing a token and some statistics regarding the mean occurrences per document in which
     * the token appears and the total number of documents in which it appears.
     *
     * @author Andrea Pasin
     */
    private static class TokenStats {
        private final String token;
        private final int totDocumentsAppeared;
        private final int tokenOccurrences;

        /**
         * Constructor for TokenStats representing a token and some of its statistics
         *
         * @param token  the token
         * @param tokenOccurrences  the total number of time the considered token appears in the collection
         * @param totDocumentsAppeared  the total number of documents in which the considered token appears
         */
        public TokenStats(@NotNull String token, int totDocumentsAppeared, int tokenOccurrences){
            this.token=token;
            this.totDocumentsAppeared=totDocumentsAppeared;
            this.tokenOccurrences=tokenOccurrences;
        }

        /**
         * Gets the total number of time the considered token appears in the collection
         * @return  the total number of time the considered token appears in the collection
         */
        public int getTokenOccurrences() {
            return tokenOccurrences;
        }

        /**
         * Gets the total number of documents in which the considered token appears
         * @return  the total number of documents in which the considered token appears
         */
        public int getTotDocsAppeared() {
            return totDocumentsAppeared;
        }

        /**
         * Gets the token
         * @return  the token
         */
        public String getToken() {
            return token;
        }
    }

    public static void main(String[] args) throws IOException {

        final String collectionPath = "PATH TO COLLECTION";
        final Analyzer analyzerWithoutStemmer=  new IntsegAnalyzer(
                IntsegAnalyzer.TokenizerType.Letter, 2, 10,
                true, null, IntsegAnalyzer.StemFilterType.EnglishMinimal, null, null);

        TermWeightingUtils.getContextInfoAndSave(analyzerWithoutStemmer,collectionPath,"PATH TO context_stats.txt");
        TermWeightingUtils.getSentencesInfoAndSave(analyzerWithoutStemmer,collectionPath,"PATH TO sentences_stats.txt");

        System.out.println("CIAO");
    }

}
