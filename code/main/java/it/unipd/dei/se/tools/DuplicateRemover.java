package it.unipd.dei.se.tools;

import java.io.*;
import java.util.ArrayList;

public class DuplicateRemover {
    public static void main(String[] args) throws IOException {
        BufferedReader reader;
        reader = new BufferedReader(new FileReader(
                "C:\\Users\\Andrea Pasin\\Downloads\\touche-task1-2022-relevance.qrels"));
        File fout = new File("C:\\Users\\Andrea Pasin\\Downloads\\removed_dup.qrels");
        ArrayList<String> entries=new ArrayList<>();
        ArrayList<String> entries1=new ArrayList<>();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fout)));
        String line;
        while ((line=reader.readLine()) != null) {
            entries.add(line);
        }

        reader.close();
        for(String entry:entries){
            boolean found=false;
            for(String e:entries1){
                if(e.substring(0,e.length()-2).equals(entry.substring(0,entry.length()-2)))
                {
                    found=true;
                }
            }
            if(!found){
                entries1.add(entry);
            }
            /*if(!entries1.contains(entry)){
                entries1.add(entry);
            }*/
        }
        for(String entry:entries1){
            bw.write(entry);
            bw.write("\n");
        }
        bw.close();
    }
}
