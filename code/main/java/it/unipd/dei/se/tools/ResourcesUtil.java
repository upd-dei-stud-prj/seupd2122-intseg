package it.unipd.dei.se.tools;

import org.apache.lucene.analysis.WordlistLoader;

import javax.validation.constraints.NotNull;
import java.io.*;

/**
 * Class used to help retrieving files from resources folder
 *
 * @author Andrea Pasin
 */
public class ResourcesUtil {

    public enum ResourcePath {
        APACHE_POS_MODEL("en-pos-maxent.bin"),
        APACHE_LANGUAGE_MODEL("langdetect-183.bin"),
        WORDNET_DATASET("synonyms.csv"),
        SEARCHER_PARAMS("SearcherParams.xml"),
        WEIGHT_PARAMS("WeightParams.xml"),
        ANALYZER_PARAMS("AnalyzerParams.xml"),
        CONTEXT_STOPLIST("stoplist-top100-context.txt"),
        SENTENCE_STOPLIST("stoplist-top100-sentence.txt"),
        CONTEXT_STATS("context_stats.txt"),
        SENTENCE_STATS("sentences_stats.txt"),
        ;

        private final String fileName;

        /**
         * @param fileName
         */
        ResourcePath(final String fileName) {
            this.fileName = fileName;
        }

        @Override
        public String toString() {
            return fileName;
        }
    }


    /**
     * Returns the file content as a String
     * @param r  the target resource from which we want to retrieve data
     * @return  the resource content
     * @throws IOException
     */
    public static String getFileContentFromName(@NotNull final ResourcePath r)throws IOException{
        ClassLoader classLoader = ResourcesUtil.class.getClassLoader();

        Reader in = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(r.toString())));

        char[] arr = new char[8 * 4096];
        StringBuilder buffer = new StringBuilder();
        int numCharsRead;
        while ((numCharsRead = in.read(arr, 0, arr.length)) != -1) {
            buffer.append(arr, 0, numCharsRead);
        }
        in.close();
        return buffer.toString();

    }

    /**
     * Returns an InputStream to the resource
     * @param r  the target resource from which we want to retrieve data
     * @return  the InputStream to the resource
     * @throws IOException
     */
    public static InputStream getFileInputStream(@NotNull final ResourcePath r)throws IOException{
        ClassLoader classLoader = ResourcesUtil.class.getClassLoader();

        return classLoader.getResourceAsStream(r.toString());

    }
}
