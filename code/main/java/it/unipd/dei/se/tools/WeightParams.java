package it.unipd.dei.se.tools;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Class used to define the weights of the searcher following the xml file
 *
 * @author Andrea Pasin
 */
public class WeightParams {
    @JacksonXmlProperty
    private float noun; //the weight for a noun (it should be greater or equal than the verb weight by a linguistic point of view)
    @JacksonXmlProperty
    private float not_noun; //the weight for a verb
    @JacksonXmlProperty
    private float synonym;  //the starting weight for a synonym
    @JacksonXmlProperty
    private float decrement_synonym;    //how much should we decrement the weight after each synonym -> only the first ones are really useful
    @JacksonXmlProperty
    private float context_query_boost;    //how much should we boost the weight of the query considering only the contexts
    @JacksonXmlProperty
    private float sentence_query_boost;    //how much should we boost the weight of the query considering only the sentences

    public float getNoun() {
        return noun;
    }

    public void setNoun(float noun) {
        this.noun = noun;
    }

    public float getSynonym() {
        return synonym;
    }

    public void setSynonym(float synonym) {
        this.synonym = synonym;
    }

    public float getDecrement_synonym() {
        return decrement_synonym;
    }

    public void setDecrement_synonym(float decrement_synonym) {
        this.decrement_synonym = decrement_synonym;
    }

    public float getNot_noun() {
        return not_noun;
    }

    public void setNot_noun(float not_noun) {
        this.not_noun = not_noun;
    }

    public float getSentence_query_boost() {
        return sentence_query_boost;
    }

    public void setSentence_query_boost(float sentence_query_boost) {
        this.sentence_query_boost = sentence_query_boost;
    }

    public float getContext_query_boost() {
        return context_query_boost;
    }

    public void setContext_query_boost(float context_query_boost) {
        this.context_query_boost = context_query_boost;
    }
}