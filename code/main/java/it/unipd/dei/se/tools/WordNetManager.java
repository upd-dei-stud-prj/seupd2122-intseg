package it.unipd.dei.se.tools;


import javax.validation.constraints.NotNull;
import java.io.*;
import java.util.*;

/*
WordNet 3.0 license:
WordNet Release 3.0
This software and database is being provided to you, the LICENSEE, by Princeton University under the following license.
By obtaining, using and/or copying this software and database, you agree that you have read, understood, and will comply with these terms and conditions.:
Permission to use, copy, modify and distribute this software and database and its documentation for any purpose and without fee or royalty is hereby granted, provided that you agree to comply with the following copyright notice and statements, including the disclaimer, and that the same appear on ALL copies of the software, database and documentation, including modifications that you make for internal use or for distribution.
WordNet 3.0 Copyright 2006 by Princeton University. All rights reserved.
THIS SOFTWARE AND DATABASE IS PROVIDED "AS IS" AND PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED.
BY WAY OF EXAMPLE, BUT NOT LIMITATION, PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES OF MERCHANT- ABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE LICENSED SOFTWARE, DATABASE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
The name of Princeton University or Princeton may not be used in advertising or publicity pertaining to distribution of the software and/or database.
Title to copyright in this software, database and any associated documentation shall at all times remain with Princeton University and LICENSEE agrees to preserve same.
 */

/**
 * Class that contains an HashMap used to store and retrieve word synonyms and word type based on the
 * WordNet dataset. More information on the WordNet project can be found at this
 * <a href="https://wordnet.princeton.edu/">link</a>.
 * It requires ~15 MB of RAM to store the whole dataset in memory.
 *
 * @author Andrea Pasin
 */
@NotNull
public class WordNetManager {

    /**
     * An enumeration of possible TAG that can be used as WordNet tags.
     * Some tags are grouped together for simplicity.
     */
    public enum TAG {
        NOUN("noun"),
        VERB("verb"),
        ADJECTIVE("adjective"),
        ADVERB("adverb");

        private final String value;

        TAG(@NotNull final String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }



    private final Reader datasetPath;
    private final HashMap<WordNetToken,String[]> wordNetTokens;

    /**
     * Constructor for the {@link WordNetManager} class
     *
     * @param datasetPath  the path to the WordNet dataset in csv format
     */
    public WordNetManager(@NotNull final ResourcesUtil.ResourcePath datasetPath) throws IOException{
        this.datasetPath=new InputStreamReader(ResourcesUtil.getFileInputStream(datasetPath));
        wordNetTokens=new HashMap<>();
        init();
    }

    /**
     * Initializes the {@link WordNetManager} HashMap of words by parsing the whole
     * WordNet dataset
     *
     * @throws IOException
     */
    private void init() throws IOException {
        if(!wordNetTokens.isEmpty()){
            return;
        }
        BufferedReader br=new BufferedReader(datasetPath);
        System.out.println("Starting reading through the WordNet Dataset...");
        String line;
        line=br.readLine(); //skip first line since it defines field
        while ((line=br.readLine()) != null) {
            String[] fields=line.split(",");
            String[] synonyms=fields[2].split("[;|]+");
            TAG tag=null;
            switch (fields[1]) {
                case "adjective", "satellite" -> tag = TAG.ADJECTIVE;
                case "noun" -> tag = TAG.NOUN;
                case "adverb" -> tag = TAG.ADVERB;
                case "verb" -> tag = TAG.VERB;
            }
            WordNetToken wordNetToken= new WordNetToken(fields[0], tag);
            wordNetTokens.put(wordNetToken,synonyms);
        }
        System.out.println("Ended reading through the WordNet Dataset.");
    }

    /**
     * Gets a list of synonyms for the given word according to the WordNet dataset
     *
     * @param word  the word for which we want to retrieve synonyms
     * @param tag  the tag of the word to find
     * @return  the list of synonyms for the given word in input
     */
    public final String[] getSynonyms(@NotNull final String word,final TAG tag){
        if(tag==null){
            return new String[0];
        }
        return wordNetTokens.getOrDefault(new WordNetToken(word, tag),new String[0]);
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("{");
        int i=0;
        for (WordNetToken key : wordNetTokens.keySet()) {
            stringBuilder.append(key.toString()).append(Arrays.toString(wordNetTokens.get(key)));
            stringBuilder.append("\n");
            i++;
            if(i>100){
                break;
            }
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    /**
     * Inner class used in the HashMap to store the WordNet words in a compact way
     *
     * @author Andrea Pasin
     */
    private static class WordNetToken {
        private final String token;
        private final TAG tag;

        /**
         * Constructor for the WordNetToken class
         *
         * @param token  the token
         * @param tag  the tag of token following the WordNet tags
         */
        public WordNetToken(@NotNull final String token,@NotNull final TAG tag){
            this.tag=tag;
            this.token=token;
        }

        /**
         * Returns the tag of the token
         *
         * @return  the tag of the token
         */
        public final TAG getTag() {
            return tag;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            WordNetToken that = (WordNetToken) o;
            return Objects.equals(token, that.token) && Objects.equals(tag, that.tag);
        }

        @Override
        public int hashCode() {
            return Objects.hash(token, tag.toString());
        }

        @Override
        public String toString() {
            return "WordNetToken{" +
                    "token='" + token + '\'' +
                    ", tag=" + tag.toString() +
                    '}';
        }
    }

    /**
     * Just for testing purposes
     */
    public static void main (String[] args){
        WordNetManager wordNetManager=null;
        try {
            File file = new File("code\\main\\resources\\synonyms.csv");
            String absolutePath = file.getAbsolutePath();
            System.out.println(absolutePath);
            wordNetManager = new WordNetManager(ResourcesUtil.ResourcePath.WORDNET_DATASET);        //add your path to the WordNet dataset like C:\...\dataset.csv
                                                                      //TODO: use the resource folder directly!
        }
        catch (Exception ex){
            System.out.println("wrong path provided!"+ex);
        }
        if(wordNetManager==null){
            return;
        }
        System.out.println(wordNetManager);
        String[] synonyms=wordNetManager.getSynonyms("acerb",TAG.ADJECTIVE);
        for(String synonym:synonyms){
            System.out.println(synonym);
        }

    }
}
