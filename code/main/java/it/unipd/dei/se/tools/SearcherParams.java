package it.unipd.dei.se.tools;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Class used to define the searcher params based on xml
 *
 * @author Andrea Pasin
 */
public class SearcherParams {
    @JacksonXmlProperty
    private String ibm_api_key; //the key to use Project Debater
    @JacksonXmlProperty
    private int sentences_multiplier;   //how many sentences should be retrieved when applying the query only on sentences.
                                        //(e.g. if set to 30 then 30k sentences would be used to obtain the final best 1000 couples context-sentence)
    @JacksonXmlProperty
    private int contexts_multiplier;    //how many contexts should be retrieved when applying the query only on contexts; notice that corresponding sentences will be extracted
                                        //there can be as many as 30 sentences for each context!
                                        //(e.g. if set to 30 then 30k contexts would be used to obtain the final best 1000 couples context-sentence)
    @JacksonXmlProperty
    private int wordnet_query_expander_and_pos_tagger;    //set to 1 if this should be used in the Searcher otherwise set to 0.
    @JacksonXmlProperty
    private int ibm_project_debater_evidence_detection;    //set to 1 if this should be used in the Searcher otherwise set to 0.
    @JacksonXmlProperty
    private int icoefficient_query_weighting;    //set to 1 if this should be used in the Searcher otherwise set to 0. This and TFIDF_QUERY_WEIGHTING are mutually exclusive!
    @JacksonXmlProperty
    private int tfidf_query_weighting;    //set to 1 if this should be used in the Searcher otherwise set to 0. This and ICOEFFICIENT_QUERY_WEIGHTING are mutually exclusive!
    @JacksonXmlProperty
    private int title_and_description;    //set to 1 if this should be used in the Searcher otherwise set to 0.
    @JacksonXmlProperty
    private WeightParams weight_params;    //the weight parameters.

    public int getWordnet_query_expander_and_pos_tagger() {
        return wordnet_query_expander_and_pos_tagger;
    }

    public void setWordnet_query_expander_and_pos_tagger(int wordnet_query_expander_and_pos_tagger) {
        this.wordnet_query_expander_and_pos_tagger = wordnet_query_expander_and_pos_tagger;
    }

    public int getIbm_project_debater_evidence_detection() {
        return ibm_project_debater_evidence_detection;
    }

    public void setIbm_project_debater_evidence_detection(int ibm_project_debater_evidence_detection) {
        this.ibm_project_debater_evidence_detection = ibm_project_debater_evidence_detection;
    }

    public int getIcoefficient_query_weighting() {
        return icoefficient_query_weighting;
    }

    public void setIcoefficient_query_weighting(int icoefficient_query_weighting) {
        this.icoefficient_query_weighting = icoefficient_query_weighting;
    }

    public int getTfidf_query_weighting() {
        return tfidf_query_weighting;
    }

    public void setTfidf_query_weighting(int tfidf_query_weighting) {
        this.tfidf_query_weighting = tfidf_query_weighting;
    }

    public int getTitle_and_description() {
        return title_and_description;
    }

    public void setTitle_and_description(int title_and_description) {
        this.title_and_description = title_and_description;
    }

    public WeightParams getWeight_params() {
        return weight_params;
    }

    public void setWeight_params(WeightParams weight_params) {
        this.weight_params = weight_params;
    }

    public String getIbm_api_key() {
        return ibm_api_key;
    }

    public void setIbm_api_key(String ibm_api_key) {
        this.ibm_api_key = ibm_api_key;
    }

    public int getSentences_multiplier() {
        return sentences_multiplier;
    }

    public void setSentences_multiplier(int sentences_multiplier) {
        this.sentences_multiplier = sentences_multiplier;
    }

    public int getContexts_multiplier() {
        return contexts_multiplier;
    }

    public void setContexts_multiplier(int contexts_multiplier) {
        this.contexts_multiplier = contexts_multiplier;
    }
}
