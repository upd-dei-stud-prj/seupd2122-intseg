package it.unipd.dei.se.indexer;


import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

/**
 * Class used to define a customized sentence value field
 *
 * @author Neemol Rajkumari
 * @author Andrea Pasin
 */
public class SentenceValueField extends Field {
    /**
     * The type of the document sentence value field
     */
    private static final FieldType SENTENCE_VALUE_TYPE = new FieldType();

    static {
        SENTENCE_VALUE_TYPE.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        SENTENCE_VALUE_TYPE.setTokenized(true);
        SENTENCE_VALUE_TYPE.setStored(true);
    }

    /**
     * Create a new field for the sentence value
     *
     * @param value the contents of the sentence value
     */
    public SentenceValueField(final Reader value) {
        super(DirectoryIndexer.SENTENCE_VALUE_FIELD, value, SENTENCE_VALUE_TYPE);
    }

    /**
     * Create a new field for the body sentence value
     *
     * @param value the contents of the sentence value
     */
    public SentenceValueField(final String value) {
        super(DirectoryIndexer.SENTENCE_VALUE_FIELD, value, SENTENCE_VALUE_TYPE);
    }
}
