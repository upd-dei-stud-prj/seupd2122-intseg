package it.unipd.dei.se.indexer;


import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

/**
 * Class used to define a customized stance field
 *
 * @author Neemol Rajkumari
 * @author Andrea Pasin
 */
public class StanceField extends Field {
    /**
     * The type of the document stance field
     */
    private static final FieldType STANCE_TYPE = new FieldType();

    static {
        STANCE_TYPE.setIndexOptions(IndexOptions.DOCS);
        STANCE_TYPE.setTokenized(false);
        STANCE_TYPE.setStored(true);
    }

    /**
     * Create a new field for the stance
     *
     * @param value the contents of the stance
     */
    public StanceField(final Reader value) {
        super(DirectoryIndexer.STANCE_FIELD, value, STANCE_TYPE);
    }

    /**
     * Create a new field for the stance
     *
     * @param value the contents of the stance
     */
    public StanceField(final String value) {
        super(DirectoryIndexer.STANCE_FIELD, value, STANCE_TYPE);
    }
}
