package it.unipd.dei.se.indexer;

import it.unipd.dei.se.parser.ParsedDocument;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

/**
 * Class used to define a customized sentence ID field
 *
 * @author Neemol Rajkumari
 * @author Andrea Pasin
 */
public class SentenceIDField extends Field {
    /**
     * The type of the document sentence id field
     */
    private static final FieldType SENTENCE_ID_TYPE = new FieldType();

    static {
        SENTENCE_ID_TYPE.setIndexOptions(IndexOptions.DOCS);
        SENTENCE_ID_TYPE.setTokenized(false);
        SENTENCE_ID_TYPE.setStored(true);
    }

    /**
     * Create a new field for the sentence id
     *
     * @param value the contents of the sentence id
     */
    public SentenceIDField(final Reader value) {
        super(DirectoryIndexer.SENTENCE_ID_FIELD, value, SENTENCE_ID_TYPE);
    }

    /**
     * Create a new field for the sentence id
     *
     * @param value the contents of the sentence id
     */
    public SentenceIDField(final String value) {
        super(DirectoryIndexer.SENTENCE_ID_FIELD, value, SENTENCE_ID_TYPE);
    }
}
