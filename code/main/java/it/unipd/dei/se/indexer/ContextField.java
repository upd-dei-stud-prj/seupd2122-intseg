package it.unipd.dei.se.indexer;

import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

/**
 * Represents a {@link Field} for containing the context of a document.
 * <p>
 * It is a tokenized field, not stored, keeping only document ids (see {@link
 * IndexOptions#DOCS} in order to minimize the space occupation.
 *
 * @author Neemol Rajkumari
 * @version 1.00
 * @since 1.00
 */
public class ContextField extends Field {

    /**
     * The type of the document context field
     */
    private static final FieldType BODY_TYPE = new FieldType();

    static {
        BODY_TYPE.setIndexOptions(IndexOptions.DOCS);
        BODY_TYPE.setTokenized(true);
        BODY_TYPE.setStored(false);
    }


    /**
     * Create a new field for the context
     *
     * @param value the contents of the context
     */
    public ContextField(final Reader value) {
        super(DirectoryIndexer.CONTEXT_FIELD, value, BODY_TYPE);
    }

    /**
     * Create a new field for the context
     *
     * @param value the contents of the context
     */
    public ContextField(final String value) {
        super(DirectoryIndexer.CONTEXT_FIELD, value, BODY_TYPE);
    }

}
