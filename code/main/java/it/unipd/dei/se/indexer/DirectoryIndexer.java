package it.unipd.dei.se.indexer;

import it.unipd.dei.se.analyzer.IntsegAnalyzer;
import it.unipd.dei.se.parser.IntsegParser;
import it.unipd.dei.se.parser.ParsedDocument;
import it.unipd.dei.se.tools.ApacheNLPManager;
import it.unipd.dei.se.tools.ResourcesUtil;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Indexes documents processing a whole directory tree.
 *
 * @author Neemol Rajkumari
 * @version 1.00
 * @since 1.00
 */
public class DirectoryIndexer {

    //These fields are using when indexing
    public final static String SENTENCE_ID_FIELD="sentence_id";
    public final static String CONTEXT_FIELD="context";
    public final static String SENTENCE_VALUE_FIELD="sentence";
    public final static String STANCE_FIELD="stance";

    /**
     * One megabyte
     */
    private static final int MBYTE = 1024 * 1024;

    /**
     * The index writer to the contexts' folder.
     */
    private final IndexWriter writer;

    /**
     * The index writer to the sentences' folder.
     */
    private final IndexWriter writer1;


    /**
     * The path to the collection.
     */
    private final Path collectionPath;

    /**
     * The charset used for encoding documents.
     */
    private final Charset cs;

    /**
     * The total number of documents expected to be indexed.
     */
    private final long expectedDocs;

    /**
     * The start instant of the indexing.
     */
    private final long start;

    /**
     * The total number of indexed files.
     */
    private long filesCount;

    /**
     * The total number of indexed documents.
     */
    private long docsCount;

    /**
     * The total number of indexed bytes
     */
    private long bytesCount;

    /**
     * Creates a new indexer.
     *
     * @param contextAnalyzer        the {@code Analyzer} to be used for context.
     * @param sentenceAnalyzer        the {@code Analyzer} to be used for sentences.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param collectionPath        the path to the collection
     * @param charsetName     the name of the charset used for encoding documents.
     * @param expectedDocs    the total number of documents expected to be indexed.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public DirectoryIndexer(final Analyzer contextAnalyzer, final Analyzer sentenceAnalyzer, final Similarity similarity, final int ramBufferSizeMB, final String indexPath, final String collectionPath,
                            final String charsetName, final long expectedDocs) {


        if (contextAnalyzer == null||sentenceAnalyzer==null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(contextAnalyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);
        iwc.setUseCompoundFile(true);
        final IndexWriterConfig iwc1 = new IndexWriterConfig(sentenceAnalyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);
        iwc.setUseCompoundFile(true);

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }


        final Path indexDirContextes = Paths.get(indexPath+"\\indexed_contexts");
        final Path indexDirSentences = Paths.get(indexPath+"\\indexed_sentences");

        // if the directory does not already exist, create it
        if (Files.notExists(indexDirContextes)) {
            try {
                Files.createDirectory(indexDirContextes);
            } catch (Exception e) {
                throw new IllegalArgumentException(
                        String.format("Unable to create directory %s: %s.", indexDirContextes.toAbsolutePath(),
                                e.getMessage()), e);
            }
        }
        if (Files.notExists(indexDirSentences)) {
            try {
                Files.createDirectory(indexDirSentences);
            } catch (Exception e) {
                throw new IllegalArgumentException(
                        String.format("Unable to create directory %s: %s.", indexDirSentences.toAbsolutePath(),
                                e.getMessage()), e);
            }
        }

        if (!Files.isWritable(indexDirContextes)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be written.", indexDirContextes.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDirContextes)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the index.",
                    indexDirContextes.toAbsolutePath()));
        }
        if (!Files.isWritable(indexDirSentences)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be written.", indexDirSentences.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDirSentences)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the index.",
                    indexDirSentences.toAbsolutePath()));
        }

        if (collectionPath == null) {
            throw new NullPointerException("Collection path cannot be null.");
        }

        if (collectionPath.isEmpty()) {
            throw new IllegalArgumentException("Collection path cannot be empty.");
        }

        this.collectionPath = Paths.get(collectionPath);

        if (charsetName == null) {
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
        }

        if (expectedDocs <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        this.filesCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDirContextes), iwc);
            writer1=new IndexWriter(FSDirectory.open(indexDirSentences),iwc1);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index writer in directory %s: %s.",
                    indexDirContextes.toAbsolutePath(), e.getMessage()), e);
        }

        this.start = System.currentTimeMillis();

    }

    /**
     * Indexes the documents.
     *
     * @throws IOException if something goes wrong while indexing.
     */
    public void index() throws IOException {

        System.out.printf("%n#### Start indexing ####%n");
        //ApacheNLPManager.init(Constants.RELATIVE_PATH_TO_APACHE_POS_TAGGER_MODEL, Constants.RELATIVE_PATH_TO_APACHE_LANGUAGE_DETECTOR_MODEL);
        ApacheNLPManager.init(ResourcesUtil.ResourcePath.APACHE_POS_MODEL,ResourcesUtil.ResourcePath.APACHE_LANGUAGE_MODEL);
        //Reader reader = new FileReader(collectionPath.toAbsolutePath().toString());

        IntsegParser dp = new IntsegParser(collectionPath.toAbsolutePath().toString());

        bytesCount += Files.size(collectionPath);

        filesCount += 1;

        //Document doc = null;

        LinkedHashMap<String, String> temp;
        LinkedHashMap<String, String> temp1;
        Set<String> key1;
        Set<String> key2;
        for (ParsedDocument pd : dp)
        {



            Document documentContext=new Document();
            documentContext.add(new ContextField(pd.get_context()));

            temp= pd.get_prem_id_body();
            key1=temp.keySet();
            for(String key:key1){
                if(key.equals("Sf01770ed-A8a3dfbc1__PREMISE__1")){
                    continue;
                    //THIS THROWS STACKOVEFRLOWERROR BECAUSE OF A VERY BAD DOCUMENT WHEN USING APACHE OPEN NLP...
                }
                /*if(!ApacheNLPManager.isEnglish(temp.get(key))){
                    continue;
                }*/

                Document doc = new Document();
                //add the document stance
                doc.add(new StanceField(pd.get_prem_stance()));
                //add the sentence identifier
                doc.add(new SentenceIDField(key));
                //add the sentence value
                doc.add(new SentenceValueField(temp.get(key)));

                //index sentence
                writer1.addDocument(doc);

                docsCount++;
                if (docsCount % 10000 == 0) {
                    System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                            docsCount, filesCount, bytesCount / MBYTE,
                            (System.currentTimeMillis() - start) / 1000);
                }
                documentContext.add(new SentenceIDField(key));
            }

            temp1= pd.get_conc_id_body();
            key2=temp1.keySet();
            for(String key:key2){

                /*if(!ApacheNLPManager.isEnglish(temp1.get(key))){
                    continue;
                }*/
                Document doc = new Document();
                //add the document stance
                doc.add(new StanceField(pd.get_prem_stance()));
                //add the sentence identifier
                doc.add(new SentenceIDField(key));
                //add the sentence value
                doc.add(new SentenceValueField(temp1.get(key)));

                //index sentence
                writer1.addDocument(doc);

                docsCount++;
                if (docsCount % 10000 == 0) {
                    System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                            docsCount, filesCount, bytesCount / MBYTE,
                            (System.currentTimeMillis() - start) / 1000);
                }

                documentContext.add(new SentenceIDField(key));
            }
            //index context
            writer.addDocument(documentContext);
        }

        writer.commit();
        writer.close();

        writer1.commit();
        writer1.close();

        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
        }

        System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
                bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

        System.out.printf("#### Indexing complete ####%n");
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 4096;
        //final String collectionPath = "PATH TO THE COLLECTION";
        //final String indexPath = "PATH TO WHERE WE WANT TO INDEX FILES";
        final String collectionPath = "PATH TO COLLECTION";
        final String indexPath = "PATH TO INDEX";

        //final String extension = "csv";
        final int expectedDocs = 365408;
        final String charsetName = "ISO-8859-1";

        IntsegAnalyzer intsegAnalyzerContext = new IntsegAnalyzer(
                IntsegAnalyzer.TokenizerType.Standard,  2, 10,
                true, null, IntsegAnalyzer.StemFilterType.EnglishMinimal, null, null);
        IntsegAnalyzer intsegAnalyzerSentence = new IntsegAnalyzer(
                IntsegAnalyzer.TokenizerType.Standard,  2, 10,
                true, null, IntsegAnalyzer.StemFilterType.EnglishMinimal, null, null);

        DirectoryIndexer i = new DirectoryIndexer(intsegAnalyzerContext, intsegAnalyzerSentence, new LMDirichletSimilarity(), ramBuffer, indexPath, collectionPath,
                charsetName, expectedDocs);

        i.index();



    }

}