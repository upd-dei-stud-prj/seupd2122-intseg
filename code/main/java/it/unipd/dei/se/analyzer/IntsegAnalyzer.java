package it.unipd.dei.se.analyzer;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import it.unipd.dei.se.searcher.ISearcher;
import it.unipd.dei.se.tools.AnalyzerParams;
import it.unipd.dei.se.tools.ResourcesUtil;
import it.unipd.dei.se.tools.SearcherParams;
import it.unipd.dei.se.tools.WordNetManager;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LetterTokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishMinimalStemFilter;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static it.unipd.dei.se.analyzer.AnalyzerUtil.consumeTokenStream;
import static it.unipd.dei.se.analyzer.AnalyzerUtil.loadStopList;

/**
 * The Analyzer in our IR System. It can be set with different parameters to allow to fine-tune it.
 * @author Mozil Sohail
 * @version 1.0
 * @since 1.0
 */
public class IntsegAnalyzer extends Analyzer {

	public enum TokenizerType
    {
        Whitespace,
        Letter,
        Standard,


    }

    public enum StemFilterType
    {
        EnglishMinimal,
        Porter,
        K,
        //Lovins
    }

    private final TokenizerType tokenizerType;

    private final  StemFilterType stemFilterType;


    //params for length Filter
    private final  Integer minLength;
    private final  Integer maxLength;

    private final  boolean isEnglishPossessiveFilter;

    private final  String stopFilterListName; //include .txt at the end like "smart.txt"

    private final  Integer nGramFilterSize; //set to null if don't want to use

    private final  Integer shingleFilterSize;  //set to null if don't want to use

        
    
    /**
	 * Creates a new instance of the analyzer.
	 */
	public IntsegAnalyzer(TokenizerType tokenizerType, int minLength, int maxLength,
	 boolean isEnglishPossessiveFilter, String stopFilterListName, StemFilterType stemFilterType, 
	 Integer nGramFilterSize, Integer shingleFilterSize) 
	 {
		super();
		
		this.tokenizerType = tokenizerType;

		
		this.minLength = minLength;
		this.maxLength = maxLength;

		this.isEnglishPossessiveFilter = isEnglishPossessiveFilter;

		this.stopFilterListName = stopFilterListName;

		this.stemFilterType = stemFilterType;

		this.nGramFilterSize = nGramFilterSize;

		this.shingleFilterSize = shingleFilterSize;

	}

    /**
     * Initializes the Analyzer following the AnalyzerParams.xml file and the Stoplist file
     * @param paramsPath  the path to the AnalyzerParams.xml file
     * @param stopListName  the name of the Stoplist file
     */
    public IntsegAnalyzer(@NotNull final String paramsPath, @NotNull final ResourcesUtil.ResourcePath stopListName)
    {
        super();
        try {
            XmlMapper xmlMapper = new XmlMapper();
            String readContent = Files.readString(Path.of(paramsPath));
            AnalyzerParams paramsXml = xmlMapper.readValue(readContent, AnalyzerParams.class);
            if(paramsXml.getUse_english_possessive_filter()==1){
                this.isEnglishPossessiveFilter=true;
            }
            else{
                this.isEnglishPossessiveFilter=false;
            }
            if(paramsXml.getUse_stop_lists()==1){
                this.stopFilterListName=stopListName.toString();
            }
            else{
                this.stopFilterListName=null;
            }
            this.nGramFilterSize=paramsXml.getN_gram_filter_size();
            this.shingleFilterSize=paramsXml.getShingle_filter_size();
            this.minLength=paramsXml.getMin_length();
            this.maxLength=paramsXml.getMax_length();
            switch (paramsXml.getStem_filter()){
                case "Porter" -> this.stemFilterType=StemFilterType.Porter;
                case "Krovetz" -> this.stemFilterType=StemFilterType.K;
                case "EnglishMinimal" -> this.stemFilterType=StemFilterType.EnglishMinimal;
                default -> throw new IllegalArgumentException("Bad initialization of IntsegAnalyzer through xml file");
            }
            switch (paramsXml.getTokenizer_type()){
                case "Whitespace" -> this.tokenizerType=TokenizerType.Whitespace;
                case "Standard" -> this.tokenizerType=TokenizerType.Standard;
                case "Letter" -> this.tokenizerType=TokenizerType.Letter;
                default -> throw new IllegalArgumentException("Bad initialization of IntsegAnalyzer through xml file");
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Params file invalid!");
        }
    }


	@Override
	protected TokenStreamComponents createComponents(String fieldName) {

		Tokenizer source = null;
        TokenStream tokens = null;

        switch(tokenizerType)
        {
            case Whitespace:
                source = new WhitespaceTokenizer();
                break;

            case Letter:
            source = new LetterTokenizer();
            break;

            case Standard:
            source = new StandardTokenizer();
            break;
        }

        tokens = new LowerCaseFilter(source);
        
        if(minLength != null && maxLength != null)
        {
            tokens = new LengthFilter(tokens, minLength, maxLength);
        }
		
        if(isEnglishPossessiveFilter)
        {
            tokens = new EnglishPossessiveFilter(tokens);
        }
		
        if(stopFilterListName != null)
        {
            tokens = new StopFilter(tokens, loadStopList(stopFilterListName));
        }
		
        switch(stemFilterType)
        {
            case EnglishMinimal:
                tokens = new EnglishMinimalStemFilter(tokens);
                break;

            case Porter:
                tokens = new PorterStemFilter(tokens);
                break;

            case K:
                tokens = new KStemFilter(tokens);
                break;

            /*case Lovins:
                tokens = new LovinsStemFilter(tokens);
                break;*/
        }
		
        if(nGramFilterSize != null)
        {
            tokens = new NGramTokenFilter(tokens, nGramFilterSize);
        }
		
        if(shingleFilterSize != null)
        {
            tokens = new ShingleFilter(tokens, shingleFilterSize);
        }

		return new TokenStreamComponents(source, tokens);
	}

	@Override
	protected Reader initReader(String fieldName, Reader reader) {
		// return new HTMLStripCharFilter(reader);

		return super.initReader(fieldName, reader);
	}

	@Override
	protected TokenStream normalize(String fieldName, TokenStream in) {
		return new LowerCaseFilter(in);
	}

	/**
	 * Main method of the class.
	 *
	 * @param args command line arguments.
	 *
	 * @throws IOException if something goes wrong while processing the text.
	 */
	public static void main(String[] args) throws IOException {

		
		//final String text = "I now live in Rome where I met my wife Alice back in 2010 during a beautiful afternoon
		// . " + "Occasionally, I fly to New York to visit the United Nations where I would like to work. The last " + "time I was there in March 2019, the flight was very inconvenient, leaving at 4:00 am, and expensive," + " over 1,500 dollars.";
		final String text = "For the first time, in its 800 academic year, University of Padua has a female " +
				"Rector.";

		
        //make a custom analyzer by passing params of components in the constructor
        //if you don't want to use any component, pass null
		IntsegAnalyzer intsegAnalyzer = new IntsegAnalyzer(
			TokenizerType.Standard, 2, 10,
		    true, null, StemFilterType.EnglishMinimal, null, null);
		

		consumeTokenStream(intsegAnalyzer, text);
		
	}
}