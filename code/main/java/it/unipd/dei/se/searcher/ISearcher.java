package it.unipd.dei.se.searcher;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import it.unipd.dei.se.analyzer.IntsegAnalyzer;
import it.unipd.dei.se.indexer.DirectoryIndexer;
import it.unipd.dei.se.tools.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;


/**
 * Searcher for the Touché Task 1
 *
 * @author Andrea Pasin
 */
public class ISearcher {
    /**
     * Class used to store the parameters for the {@link  ISearcher}
     */
    public static final class Params{

        private final Set<Param> params;

        /**
         * Enum utility representing all the possible parameters for the {@code ISearcher}
         */
        public enum Param{
            /**
             * Apply the IBM Project Debater (ONLY USED FOR RE-RANKING PURPOSES BECAUSE OF API LIMITS)
             */
            IBM_PROJECT_DEBATER_EVIDENCE_DETECTION,

            /**
             * Apply WORDNET for synonyms query expansion
             */
            WORDNET_QUERY_EXPANDER_AND_POS_TAGGER,

            /**
             * Apply the INTSEG coefficient to give discriminant weight to tokens in query
             */
            ICOEFFICIENT_QUERY_WEIGHTING,

            /**
             * Apply the TFIDF weighting to give discriminant weight to tokens in query
             */
            TFIDF_QUERY_WEIGHTING,

            /**
             * Formulate queries based on both title and description (otherwise only title is used)
             */
            TITLE_AND_DESCRIPTION,
        }

        /**
         * Constructor for the {@code Params} class
         *
         * @param params  the list of {@code Params.Param.value} representing how the {@code ISearcher} should handle the
         *                search process
         */
        public Params(@NotNull final List<Param> params){
            this.params=Set.copyOf(params);
        }

        /**
         * Check if a given parameter is present
         *
         * @param param  the {@code Params.Param.value} representing how the {@code ISearcher} should handle the query
         *                expansion part and re-ranking
         * @return  {@code true} if the given {@code Params.Param.value} is present
         */
        public boolean checkParam(Param param){
            return params.contains(param);
        }
    }


    /**
     * The Analyzer used when indexing the collection
     */
    private final Analyzer analyzerWithStemmer;

    /**
     * An Analyzer without stemmer or only with EnglishMinimal Stemmer to process synonyms and weights for tokens
     */
    private final Analyzer analyzerWithoutStemmer;

    /**
     * The identifier of the run
     */
    private final String runID;

    /**
     * The run to be written
     */
    private final PrintWriter run;

    /**
     * The index reader for Contexts
     */
    private final IndexReader readerContexts;

    /**
     * The index reader for Sentences
     */
    private final IndexReader readerSentences;

    /**
     * The API-Key for Project Debater
     */
    private final String projectDebaterKey;

    /**
     * The index searcher for Contexts
     */
    private final IndexSearcher searcherContexts;

    /**
     * The index searcher for Sentences
     */
    private final IndexSearcher searcherSentences;

    /**
     * The path to the topicsFile
     */
    private final String topicsFile;

    /**
     * The list of topics that will be used to retrieve documents
     */
    private final List<Topic> topicsProcessed;

    /**
     * The maximum number of documents to retrieve
     */
    private final int maxDocsRetrieved;

    /**
     * The parameters to do query expansion
     */
    private final Params params;

    /**
     * The weightingUtils class for Sentences
     */
    private final TermWeightingUtils sentencesWeighting;

    /**
     * The weightingUtils class for Contexts
     */
    private final TermWeightingUtils contextWeighting;

    /**
     * The weightParams class
     */
    private final WeightParams weightParams;

    /**
     * The multiplier for Sentences
     */
    private final int sentences_multiplier;

    /**
     * The multiplier for Contexts
     */
    private final int contexts_multiplier;


    /**
     * Constructor for the {@link ISearcher}
     *
     * @param analyzerWithStemmer  the {@code Analyzer} to be used with Stemmer (if used)
     * @param analyzerWithoutStemmer  the {@code Analyzer} to be used without Stemmer or with EnglishMinimal Stemmer.
     *                                This is important for the WordNet synonyms query expansion part.
     *                                Stemming may prevent finding synonyms!
     * @param similarity  the {@code Similarity} to be used.
     * @param indexPath  the directory containing the indexed files
     * @param topicsFile  the file containing the topics to search for
     * @param expectedTopics  the total number of topics expected to be searched
     * @param runID  the identifier of the run to be created
     * @param runPath  the path where to store the run
     * @param maxDocsRetrieved  the maximum number of documents to be retrieved
     * @param params  the parameters to do query expansion
     * @param pathWeightParams  the path to the xml file specifying the weights to use
     * @throws NullPointerException     if any of the parameters is {@code null}
     * @throws IllegalArgumentException if any of the parameters assumes invalid values
     */
    public ISearcher(@NotNull final Analyzer analyzerWithStemmer, @NotNull final Analyzer analyzerWithoutStemmer,
                          @NotNull final Similarity similarity, @NotNull final String indexPath,
                          @NotNull final String topicsFile, final int expectedTopics, @NotNull final String runID,
                          @NotNull final String runPath, final int maxDocsRetrieved, @NotNull final Params params, @NotNull final String pathWeightParams,
                            @NotNull final String projectDebaterKey){

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }


        this.projectDebaterKey=projectDebaterKey;
        this.sentences_multiplier=100;
        this.contexts_multiplier=1;


        Path indexDir = Paths.get(indexPath+"\\indexed_contexts");

        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try {
            readerContexts = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcherContexts = new IndexSearcher(readerContexts);
        searcherContexts.setSimilarity(similarity);


        indexDir = Paths.get(indexPath+"\\indexed_sentences");

        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try {
            readerSentences = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcherSentences = new IndexSearcher(readerSentences);
        searcherSentences.setSimilarity(similarity);



        if (topicsFile.isEmpty()) {
            throw new IllegalArgumentException("Topics file cannot be empty.");
        }

        if (expectedTopics <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        //qp = new QueryParser(ParsedDocument.FIELDS.CONC_BODY, analyzer);

        if (runID.isEmpty()) {
            throw new IllegalArgumentException("Run identifier cannot be empty.");
        }

        this.runID = runID;

        if (runPath.isEmpty()) {
            throw new IllegalArgumentException("Run path cannot be empty.");
        }

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir)) {
            throw new IllegalArgumentException(
                    String.format("Run directory %s cannot be written.", runDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(runDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the run.",
                    runDir.toAbsolutePath()));
        }

        Path runFile = runDir.resolve(runID + ".txt");
        try {
            run = new PrintWriter(Files.newBufferedWriter(runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to open run file %s: %s.", runFile.toAbsolutePath(), e.getMessage()), e);
        }

        if (maxDocsRetrieved <= 0) {
            throw new IllegalArgumentException(
                    "The maximum number of documents to be retrieved cannot be less than or equal to zero.");
        }

        try {
            XmlMapper xmlMapper = new XmlMapper();

            // read file and put contents into the string
            String readContent = new String(Files.readAllBytes(Paths.get(pathWeightParams)));

            this.weightParams = xmlMapper.readValue(readContent, WeightParams.class);
        } catch (IOException e) {
            throw new IllegalArgumentException("WeightParams file invalid!");
        }


        this.maxDocsRetrieved = maxDocsRetrieved;
        this.params=params;
        this.topicsFile=topicsFile;
        this.analyzerWithoutStemmer=analyzerWithoutStemmer;
        this.analyzerWithStemmer=analyzerWithStemmer;
        this.topicsProcessed=processTopics();

        if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)&&params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
            throw new IllegalArgumentException("Cannot have both I-Coefficient and TF-IDF query weighting!");
        }
        if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)|| params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
            try{
                sentencesWeighting =new TermWeightingUtils(ResourcesUtil.ResourcePath.SENTENCE_STATS);
                contextWeighting =new TermWeightingUtils(ResourcesUtil.ResourcePath.CONTEXT_STATS);
            }catch (Exception ex){
                throw new IllegalStateException("Couldn't read file properly "+ex);
            }
        }
        else{
            sentencesWeighting =null;
            contextWeighting =null;
        }

    }


    /**
     * Constructor for the {@link ISearcher}
     *
     * @param analyzerWithStemmer  the {@code Analyzer} to be used with Stemmer (if used)
     * @param analyzerWithoutStemmer  the {@code Analyzer} to be used without Stemmer or with EnglishMinimal Stemmer.
     *                                     This is important for the WordNet synonyms query expansion part.
     *                                     Stemming may prevent finding synonyms!
     * @param similarity  the {@code Similarity} to be used.
     * @param indexPath the directory containing the indexed files to be searched
     * @param topicsFile  the file containing the topics to search for
     * @param expectedTopics  the total number of topics expected to be searched
     * @param runID  the identifier of the run to be created
     * @param runPath  the path where to store the run
     * @param maxDocsRetrieved  the maximum number of documents to be retrieved
     * @param pathSearcherParamas  the path to the xml file specifying the searcherParams to use
     * @throws NullPointerException     if any of the parameters is {@code null}
     * @throws IllegalArgumentException if any of the parameters assumes invalid values
     */
    public ISearcher(@NotNull final Analyzer analyzerWithStemmer, @NotNull final Analyzer analyzerWithoutStemmer,
                     @NotNull final Similarity similarity, @NotNull final String indexPath,
                     @NotNull final String topicsFile, final int expectedTopics, @NotNull final String runID,
                     @NotNull final String runPath, final int maxDocsRetrieved, @NotNull final String pathSearcherParamas){

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        if (pathSearcherParamas.isEmpty()) {
            throw new IllegalArgumentException("pathSearcherParams path cannot be empty.");
        }

        try {
            XmlMapper xmlMapper = new XmlMapper();

            // read file and put contents into the string
            String readContent = Files.readString(Path.of(pathSearcherParamas));

            SearcherParams paramsXml = xmlMapper.readValue(readContent, SearcherParams.class);
            List<Params.Param> params=new ArrayList<>();
            if(paramsXml.getIbm_project_debater_evidence_detection()==1){
                params.add(Params.Param.IBM_PROJECT_DEBATER_EVIDENCE_DETECTION);
            }
            if(paramsXml.getIcoefficient_query_weighting()==1){
                params.add(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING);
            }
            if(paramsXml.getTfidf_query_weighting()==1){
                params.add(Params.Param.TFIDF_QUERY_WEIGHTING);
            }
            if(paramsXml.getTitle_and_description()==1){
                params.add(Params.Param.TITLE_AND_DESCRIPTION);
            }
            if(paramsXml.getWordnet_query_expander_and_pos_tagger()==1){
                params.add(Params.Param.WORDNET_QUERY_EXPANDER_AND_POS_TAGGER);
            }
            this.params=new Params(params);
            this.projectDebaterKey=paramsXml.getIbm_api_key();
            this.sentences_multiplier=paramsXml.getSentences_multiplier();
            this.contexts_multiplier=paramsXml.getContexts_multiplier();
            this.weightParams=paramsXml.getWeight_params();

        } catch (IOException e) {
            throw new IllegalArgumentException("SearcherParams.xml file invalid!");
        }

        Path indexDir = Paths.get(indexPath+"\\indexed_contexts");

        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try {
            readerContexts = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcherContexts = new IndexSearcher(readerContexts);
        searcherContexts.setSimilarity(similarity);


        indexDir = Paths.get(indexPath+"\\indexed_sentences");

        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try {
            readerSentences = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcherSentences = new IndexSearcher(readerSentences);
        searcherSentences.setSimilarity(similarity);



        if (topicsFile.isEmpty()) {
            throw new IllegalArgumentException("Topics file cannot be empty.");
        }

        if (expectedTopics <= 0) {
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        //qp = new QueryParser(ParsedDocument.FIELDS.CONC_BODY, analyzer);

        if (runID.isEmpty()) {
            throw new IllegalArgumentException("Run identifier cannot be empty.");
        }

        this.runID = runID;

        if (runPath.isEmpty()) {
            throw new IllegalArgumentException("Run path cannot be empty.");
        }

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir)) {
            throw new IllegalArgumentException(
                    String.format("Run directory %s cannot be written.", runDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(runDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the run.",
                    runDir.toAbsolutePath()));
        }

        Path runFile = runDir.resolve(runID + ".txt");
        try {
            run = new PrintWriter(Files.newBufferedWriter(runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE));
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to open run file %s: %s.", runFile.toAbsolutePath(), e.getMessage()), e);
        }

        if (maxDocsRetrieved <= 0) {
            throw new IllegalArgumentException(
                    "The maximum number of documents to be retrieved cannot be less than or equal to zero.");
        }




        this.maxDocsRetrieved = maxDocsRetrieved;
        this.topicsFile=topicsFile;
        this.analyzerWithoutStemmer=analyzerWithoutStemmer;
        this.analyzerWithStemmer=analyzerWithStemmer;
        this.topicsProcessed=processTopics();

        if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)&&params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
            throw new IllegalArgumentException("Cannot have both I-Coefficient and TF-IDF query weighting!");
        }
        if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)|| params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
            try{
                sentencesWeighting =new TermWeightingUtils(ResourcesUtil.ResourcePath.SENTENCE_STATS);
                contextWeighting =new TermWeightingUtils(ResourcesUtil.ResourcePath.CONTEXT_STATS);
            }catch (Exception ex){
                throw new IllegalStateException("Couldn't read file properly "+ex);
            }
        }
        else{
            sentencesWeighting =null;
            contextWeighting =null;
        }

    }

    /**
     * Processes the topics according to the Analyzer without stemmer provided and according
     * to the parameters given
     *
     * @return  the list of processed topics
     */
    private List<Topic> processTopics(){
        List<Topic> topics=new ArrayList<>();

        //Firstly add synonyms
        if(params.checkParam(Params.Param.WORDNET_QUERY_EXPANDER_AND_POS_TAGGER)){
            topics.addAll(processWordNet());
        }
        else{
            topics.addAll(processWithoutWordNet());
        }
        return topics;
    }


    /**
     * Helper method tokenizing a given text using the {@code Analyzer} provided in the constructor
     *
     * @param text  the text to tokenize
     * @return  the List containing all the tokens with weights 1 as default in the given text and tags if the WORDNET_QUERY_EXPANDER_AND_POS_TAGGER
     * has been set
     * @throws IOException
     */
    private List<TokenWeightTag> processTextWithAnalyzer(@NotNull final String text) throws IOException{
        TokenStream stream=analyzerWithoutStemmer.tokenStream("field", new StringReader(text));
        List<String> textTokenizedRaw=new ArrayList<>();
        final CharTermAttribute tokenTerm = stream.addAttribute(CharTermAttribute.class);
        stream.reset();
        while(stream.incrementToken()){
            textTokenizedRaw.add(tokenTerm.toString());
        }
        stream.close();
        if(params.checkParam(Params.Param.WORDNET_QUERY_EXPANDER_AND_POS_TAGGER)){
            Object[] o=textTokenizedRaw.toArray();
            String[] textTokenizedRawToTag=new String[o.length];
            for(int i = 0 ; i < o.length ; i ++){
                textTokenizedRawToTag[i] = (String)o[i];
            }

            //Here we have the tokens with weights set to 1f and the corresponding tags
            return processTags(textTokenizedRawToTag);
        }
        else{
            List<TokenWeightTag> tokenWeightTags=new ArrayList<>();
            for(String s:textTokenizedRaw){
                tokenWeightTags.add(new TokenWeightTag(s,1f));
            }
            return tokenWeightTags;
        }
    }

    /**
     * Helper method exploiting the ApacheOpenNLP POS tagger to tag a given tokenized text provided in the constructor
     *
     * @param tokens  the tokenized text to tag
     * @return  the List containing all the tokens with weights 1 as default in the given text and tags
     */
    private List<TokenWeightTag> processTags(final String[] tokens){
        try {
            ApacheNLPManager.init(ResourcesUtil.ResourcePath.APACHE_POS_MODEL,ResourcesUtil.ResourcePath.APACHE_LANGUAGE_MODEL);

            return ApacheNLPManager.queryPOSTag(tokens,weightParams);
        }
        catch (Exception e){
            System.out.println("NLPTagger couldn't work! "+e);
            return null;
        }
    }

    /**
     * Helper method exploiting to obtain a list of tokenized topics without query expansion
     *
     * @return  the List of topics processed without synonyms and tags
     */
    private List<Topic> processWithoutWordNet(){
        List<Topic> topics=new ArrayList<>();

        try {
            XmlMapper xmlMapper = new XmlMapper();

            // read file and put contents into the string
            String readContent = new String(Files.readAllBytes(Paths.get(topicsFile)));

            RawTopics deserializedData = xmlMapper.readValue(readContent, RawTopics.class);
            for(RawTopic t:deserializedData.getTopics()){

                //Tokenize with the Analyzer the title
                final List<List<TokenWeightTag>> titleProcessedTokens=new ArrayList<>();
                List<TokenWeightTag> titleRawTokens=processTextWithAnalyzer(t.getTitle());
                for(TokenWeightTag tk:titleRawTokens){
                    List<TokenWeightTag> toAdd=new ArrayList<>();
                    toAdd.add(tk);
                    titleProcessedTokens.add(toAdd);
                }

                //Tokenize with the Analyzer the description
                final List<List<TokenWeightTag>> descriptionProcessedTokens=new ArrayList<>();  //the final list of tokens to pass to the topic as description
                if(params.checkParam(Params.Param.TITLE_AND_DESCRIPTION)){
                    List<TokenWeightTag> descriptionRawTokens=processTextWithAnalyzer(t.getDescription());
                    for(TokenWeightTag tk:descriptionRawTokens){
                        List<TokenWeightTag> toAdd=new ArrayList<>();
                        toAdd.add(tk);
                        descriptionProcessedTokens.add(toAdd);
                    }
                }
                Topic topic=new Topic(String.valueOf(t.getNumber()),t.getTitle(),t.getDescription(),t.getNarrative(),titleProcessedTokens,descriptionProcessedTokens);
                topics.add(topic);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicsFile, e.getMessage()), e);
        }
        return topics;
    }

    /**
     * Helper method exploiting the ApacheOpenNLP POS tagger and WordNet dataset to process a topic and
     * retrieve the tokenized title and description with tags and synonyms
     *
     * @return  the List of topics processed with synonyms and tags
     */
    private List<Topic> processWordNet(){
        List<Topic> topics=new ArrayList<>();
        WordNetManager wordNetManager;
        try{
            wordNetManager=new WordNetManager(ResourcesUtil.ResourcePath.WORDNET_DATASET);
            //wordNetManager=new WordNetManager(Constants.RELATIVE_PATH_TO_WORDNET_DATASET);
        }
        catch (Exception ex){
            return new ArrayList<>();
        }

        try {
            XmlMapper xmlMapper = new XmlMapper();

            // read file and put contents into the string
            String readContent = new String(Files.readAllBytes(Paths.get(topicsFile)));

            // deserialize from the XML into a RawTopics object
            RawTopics deserializedData = xmlMapper.readValue(readContent, RawTopics.class);
            for(RawTopic t:deserializedData.getTopics()){

                //Tokenize with the Analyzer and expand title through WordNet synonyms based on TAGs retrieved with ApacheOpenNLP
                final List<TokenWeightTag> titleRawTokens=processTextWithAnalyzer(t.getTitle());
                final List<List<TokenWeightTag>> titleProcessedTokens=new ArrayList<>();    //the final list of tokens to pass to the topic as title
                for(TokenWeightTag token:titleRawTokens){
                    final String[] synonymsRaw= wordNetManager.getSynonyms(token.getToken(), ApacheNLPManager.fromApacheToWordnet(token.getTag()));
                    List<TokenWeightTag> tokensProcessed=new ArrayList<>();
                    tokensProcessed.add(token);
                    float coef=weightParams.getSynonym();
                    for(String s:synonymsRaw){
                        tokensProcessed.add(new TokenWeightTag(s,coef));
                        coef-=weightParams.getDecrement_synonym();
                        if(coef<=0){
                            break;
                        }

                    }
                    titleProcessedTokens.add(tokensProcessed);
                }
                //Tokenize with the Analyzer and expand description through WordNet synonyms based on TAGs retrieved with ApacheOpenNLP
                final List<List<TokenWeightTag>> descriptionProcessedTokens=new ArrayList<>();  //the final list of tokens to pass to the topic as description
                if(params.checkParam(Params.Param.TITLE_AND_DESCRIPTION)){
                    final List<TokenWeightTag> descriptionRawTokens=processTextWithAnalyzer(t.getDescription());
                    for(TokenWeightTag token:descriptionRawTokens){
                        final String[] synonymsRaw=wordNetManager.getSynonyms(token.getToken(), ApacheNLPManager.fromApacheToWordnet(token.getTag()));
                        List<TokenWeightTag> tokensProcessed=new ArrayList<>();
                        tokensProcessed.add(token);
                        float coef=weightParams.getSynonym();
                        for(String s:synonymsRaw){
                            tokensProcessed.add(new TokenWeightTag(s,coef));
                            coef-=weightParams.getDecrement_synonym();
                            if(coef<=0){
                                break;
                            }
                        }
                        descriptionProcessedTokens.add(tokensProcessed);
                    }
                }
                Topic topic=new Topic(String.valueOf(t.getNumber()),t.getTitle(),t.getDescription(),t.getNarrative(),titleProcessedTokens,descriptionProcessedTokens);
                topics.add(topic);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicsFile, e.getMessage()), e);
        }
        return topics;
    }

    /**
     * This must be done in order to adapt the query to the form of the corpus after having applied
     * some sort of analyzer with a particular stemmer
     *
     * @param token  the token in the query to analyze through the analyzer with stemmer before creating the full query
     * @return  the list of token representing how the considered token has been processed
     * @throws IOException
     */
    private List<String> processAnalyzerStemmed(@NotNull final String token) throws IOException{
        TokenStream stream=analyzerWithStemmer.tokenStream("field", new StringReader(token));
        List<String> textTokenizedRaw=new ArrayList<>();
        final CharTermAttribute tokenTerm = stream.addAttribute(CharTermAttribute.class);
        stream.reset();
        while(stream.incrementToken()){
            textTokenizedRaw.add(tokenTerm.toString());
        }
        stream.close();
        return textTokenizedRaw;

    }

    private Query createSentencesQueryFromTopicAndIds(@NotNull final Topic topic)throws IOException{
        BooleanQuery.Builder bq = new BooleanQuery.Builder();
        Query query;

        for(List<TokenWeightTag> tokenSynonym: Objects.requireNonNull(topic.getTitleTokenizedWeighted())){
            for(TokenWeightTag t:tokenSynonym){
                float processedWeight=t.getWeight();
                List<String> tokenProcessed=processAnalyzerStemmed(t.getToken());
                if(tokenProcessed.size()>1){
                    if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)){
                        float meanWeight=0f;
                        for(String s:tokenProcessed){
                            meanWeight+= sentencesWeighting.calculateINTSEGcoef(s);
                        }
                        meanWeight=meanWeight/tokenProcessed.size();
                        processedWeight+=meanWeight;
                    }
                    else if(params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
                        float meanWeight=0f;
                        for(String s:tokenProcessed){
                            meanWeight+= sentencesWeighting.calculateTFIDF(s);
                        }
                        meanWeight=meanWeight/tokenProcessed.size();
                        processedWeight*=meanWeight;
                    }

                    //Create a PhraseQuery in case the token has been split
                    PhraseQuery.Builder builder = new PhraseQuery.Builder();
                    for(int i=0;i<tokenProcessed.size();i++){
                        builder.add(new Term(DirectoryIndexer.SENTENCE_VALUE_FIELD, tokenProcessed.get(i)), i);
                    }
                    Query q=builder.build();
                    BoostQuery boostQuery=new BoostQuery(q,processedWeight);
                    bq.add(boostQuery, BooleanClause.Occur.SHOULD);
                }
                else if(tokenProcessed.size()>0){
                    if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)){
                        processedWeight+= sentencesWeighting.calculateINTSEGcoef(tokenProcessed.get(0));
                    }
                    else if(params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
                        processedWeight*= sentencesWeighting.calculateTFIDF(tokenProcessed.get(0));
                    }
                    Query q = new TermQuery(new Term(DirectoryIndexer.SENTENCE_VALUE_FIELD, tokenProcessed.get(0)));
                    BoostQuery boostQuery=new BoostQuery(q,processedWeight);
                    bq.add(boostQuery, BooleanClause.Occur.SHOULD);
                }
            }
        }
        query = bq.build();
        return query;
    }


    /**
     * Helper method creating a query from a given topic
     *
     * @param topic  the topic used to create a query
     * @return  the query for the given topic
     */
    private Query createContextQueryFromTopic(@NotNull final Topic topic)throws IOException{
        BooleanQuery.Builder bq = new BooleanQuery.Builder();
        Query query;

        for(List<TokenWeightTag> tokenSynonym: Objects.requireNonNull(topic.getTitleTokenizedWeighted())){
            for(TokenWeightTag t:tokenSynonym){
                float processedWeight=t.getWeight();
                List<String> tokenProcessed=processAnalyzerStemmed(t.getToken());
                if(tokenProcessed.size()==1){
                    if(params.checkParam(Params.Param.ICOEFFICIENT_QUERY_WEIGHTING)){
                        processedWeight+= contextWeighting.calculateINTSEGcoef(tokenProcessed.get(0));
                    }
                    else if(params.checkParam(Params.Param.TFIDF_QUERY_WEIGHTING)){
                        processedWeight*= contextWeighting.calculateTFIDF(tokenProcessed.get(0));
                    }
                    Query q = new TermQuery(new Term(DirectoryIndexer.CONTEXT_FIELD, tokenProcessed.get(0)));
                    BoostQuery boostQuery=new BoostQuery(q,processedWeight);
                    bq.add(boostQuery, BooleanClause.Occur.SHOULD);
                }
            }
        }

        query = bq.build();
        return query;
    }

    /**
     * Searches the documents for the processed topics and writes the result into run.txt
     *
     * @throws IOException
     */
    public void search() throws IOException {
        final Set<String> fields = new HashSet<>();
        fields.add(DirectoryIndexer.SENTENCE_ID_FIELD);
        fields.add(DirectoryIndexer.SENTENCE_VALUE_FIELD);

        Query qContexts;
        TopDocs docsContexts;
        ScoreDoc[] sdContexts;
        HashMap<String,ScoredSentence> idContexts=new HashMap<>();
        Query qSentences;
        TopDocs docsSentences;
        ScoreDoc[] sdSentences;
        HashMap<String,ScoredSentence> idSentences=new HashMap<>();
        try {
            for (Topic t : topicsProcessed) {

                System.out.printf("Searching for topic %s.%n", t.getNumber());
                qContexts=createContextQueryFromTopic(t);
                docsContexts = searcherContexts.search(qContexts, maxDocsRetrieved*contexts_multiplier);
                sdContexts = docsContexts.scoreDocs;
                for(int i=0;i<sdContexts.length;i++){
                    for(String sentenceId: readerContexts.document(sdContexts[i].doc, fields).getValues(DirectoryIndexer.SENTENCE_ID_FIELD)){
                        idContexts.put(sentenceId,new ScoredSentence(sentenceId,sdContexts[i].score));
                    }
                }

                qSentences=createSentencesQueryFromTopicAndIds(t);
                docsSentences = searcherSentences.search(qSentences, maxDocsRetrieved*sentences_multiplier);
                sdSentences= docsSentences.scoreDocs;
                for(int i=0;i<sdSentences.length;i++){
                    String sentenceId=readerSentences.document(sdSentences[i].doc, fields).get(DirectoryIndexer.SENTENCE_ID_FIELD);
                    String sentenceValue=readerSentences.document(sdSentences[i].doc, fields).get(DirectoryIndexer.SENTENCE_VALUE_FIELD);
                    idSentences.put(sentenceId,new ScoredSentence(sentenceId,sentenceValue,sdSentences[i].score));
                }

                for(String k:idContexts.keySet()){
                    if(idSentences.containsKey(k)){
                        idSentences.put(k,new ScoredSentence(idSentences.get(k).id,idSentences.get(k).value,
                                idSentences.get(k).weight*weightParams.getSentence_query_boost()+idContexts.get(k).weight*weightParams.getContext_query_boost()));
                    }
                }
                List<ScoredSentence> scoredSentencesOrderedRaw=new ArrayList<>();
                for(String k:idSentences.keySet()){
                    scoredSentencesOrderedRaw.add(idSentences.get(k));
                }
                scoredSentencesOrderedRaw.sort(Comparator.comparingDouble(ScoredSentence::getWeight));
                Collections.reverse(scoredSentencesOrderedRaw);
                List<ScoredSentence> scoredSentencesOrdered=new ArrayList<>();
                for(int i=0;i<maxDocsRetrieved*2;i++){
                    scoredSentencesOrdered.add(scoredSentencesOrderedRaw.get(i));
                }

                reRank(t, scoredSentencesOrdered);

                run.flush();
                idContexts.clear();
                idSentences.clear();
            }
        } finally {
            run.close();
            readerContexts.close();
        }
    }


    /**
     * This is the function to test our system for 2021 topics
     * @throws IOException
     */
    public void search2021Topics() throws IOException{
        final Set<String> fields = new HashSet<>();
        fields.add(DirectoryIndexer.SENTENCE_ID_FIELD);
        fields.add(DirectoryIndexer.SENTENCE_VALUE_FIELD);

        Query qContexts;
        TopDocs docsContexts;
        ScoreDoc[] sdContexts;
        HashMap<String,ScoredSentence> idContexts=new HashMap<>();
        Query qSentences;
        TopDocs docsSentences;
        ScoreDoc[] sdSentences;
        HashMap<String,ScoredSentence> idSentences=new HashMap<>();
        try {
            for (Topic t : topicsProcessed) {

                System.out.printf("Searching for topic %s.%n", t.getNumber());
                qContexts=createContextQueryFromTopic(t);
                docsContexts = searcherContexts.search(qContexts, maxDocsRetrieved*contexts_multiplier);
                sdContexts = docsContexts.scoreDocs;
                for(int i=0;i<sdContexts.length;i++){
                    for(String sentenceId: readerContexts.document(sdContexts[i].doc, fields).getValues(DirectoryIndexer.SENTENCE_ID_FIELD)){
                        idContexts.put(sentenceId,new ScoredSentence(sentenceId,sdContexts[i].score));
                        //break;
                    }
                }

                qSentences=createSentencesQueryFromTopicAndIds(t);
                docsSentences = searcherSentences.search(qSentences, maxDocsRetrieved*sentences_multiplier);
                sdSentences= docsSentences.scoreDocs;
                for(int i=0;i<sdSentences.length;i++){
                    String sentenceId=readerSentences.document(sdSentences[i].doc, fields).get(DirectoryIndexer.SENTENCE_ID_FIELD);
                    String sentenceValue=readerSentences.document(sdSentences[i].doc, fields).get(DirectoryIndexer.SENTENCE_VALUE_FIELD);
                    idSentences.put(sentenceId,new ScoredSentence(sentenceId,sentenceValue,sdSentences[i].score));
                }

                for(String k:idContexts.keySet()){
                    if(idSentences.containsKey(k)){
                        idSentences.put(k,new ScoredSentence(idSentences.get(k).id,idSentences.get(k).value,
                                idSentences.get(k).weight*weightParams.getSentence_query_boost()+idContexts.get(k).weight*weightParams.getContext_query_boost()));
                    }
                }
                List<ScoredSentence> scoredSentencesOrderedRaw=new ArrayList<>();
                for(String k:idSentences.keySet()){
                    scoredSentencesOrderedRaw.add(idSentences.get(k));
                }
                scoredSentencesOrderedRaw.sort(Comparator.comparingDouble(ScoredSentence::getWeight));
                Collections.reverse(scoredSentencesOrderedRaw);
                List<ScoredSentence> scoredSentencesOrdered=new ArrayList<>();
                for(int i=0;i<maxDocsRetrieved*2;i++){
                    scoredSentencesOrdered.add(scoredSentencesOrderedRaw.get(i));
                }
                reRank2021Topics(t,scoredSentencesOrdered);


                run.flush();
                idContexts.clear();
                idSentences.clear();
            }
        } finally {
            run.close();
            readerContexts.close();
        }
    }


    /**
     * Re-ranking for 2021 topics
     * @throws IOException
     */
    private void reRank2021Topics(Topic t,List<ScoredSentence> scoredSentences) throws IOException{

        List<ScoredSentence> scoredSentences1=new ArrayList<>();

        for(ScoredSentence scoredSentence:scoredSentences){
            //re rank sentences based on their length
            String[] tokens=scoredSentence.getValue().split(" ");   //divide into tokens
            double coeff=Math.min(1,tokens.length/7d);  //if the sentence has more than x words...
            scoredSentences1.add(new ScoredSentence(scoredSentence.getId(), scoredSentence.getValue(), scoredSentence.getWeight() * coeff));
        }
        if(params.checkParam(Params.Param.IBM_PROJECT_DEBATER_EVIDENCE_DETECTION)){
            List<ScoredSentence> scoredSentences2=new ArrayList<>();
            List<String> sentences=new ArrayList<>();
            for(ScoredSentence scoredSentence:scoredSentences) {
                sentences.add(scoredSentence.getValue());
            }
            List<Float> score=DebaterManager.getEvidenceScores(t.getTitle(),sentences,projectDebaterKey);

            for(int i=0;i<score.size();i++){
                scoredSentences2.add(new ScoredSentence(scoredSentences1.get(i).getId(),scoredSentences1.get(i).getValue(),
                        scoredSentences1.get(i).getWeight()*score.get(i)));
            }
            scoredSentences1.clear();
            scoredSentences1.addAll(scoredSentences2);
        }
        scoredSentences1.sort(Comparator.comparingDouble(ScoredSentence::getWeight));
        Collections.reverse(scoredSentences1);  //now should be in ascending order!
        List<ScoredSentence> scoredSentences2=new ArrayList<>();
        for(int i=0;i<scoredSentences1.size();i++){
            boolean found=false;
            for(int k=i+1;k<scoredSentences1.size();k++){
                String[] docId1=scoredSentences1.get(i).getId().split("_");
                String[] docId2=scoredSentences1.get(k).getId().split("_");
                if(docId1[0].equals(docId2[0])){
                    found=true;
                }
            }
            if(!found){
                scoredSentences2.add(scoredSentences1.get(i));
            }
        }
        for(int i=0;i<1000&&i<scoredSentences2.size();i++){
            String[] docId=scoredSentences2.get(i).getId().split("_");
            run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getNumber(),"Q0", docId[0], i, scoredSentences2.get(i).weight, runID);
        }
        run.flush();

    }




    /**
     * Re-ranks the sentences retrieved according to some weights and parameters passed as input to this {@code ISearcher}
     * @param t  the considered topic
     * @param scoredSentencesStart  the list of scoredSentences retrieved from the query
     * @throws IOException
     */
    private void reRank(Topic t,List<ScoredSentence> scoredSentencesStart) throws IOException{

        List<ScoredSentence> scoredSentences=new ArrayList<>();
        List<String> sentencesToProcess=new ArrayList<>();
        for(int i=0;i<scoredSentencesStart.size();i++) {
            sentencesToProcess.add(scoredSentencesStart.get(i).getValue());
        }
        long startTime = System.currentTimeMillis();
        List<Float> values=DebaterManager.getProConScore(t.title,sentencesToProcess,projectDebaterKey);
        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.out.println("Pro/Con runtime: " + (duration/1000) + " seconds");
        for(int i=0;i<values.size();i++){

            String value=scoredSentencesStart.get(i).getValue();
            String id=scoredSentencesStart.get(i).getId();
            float weight=(float)scoredSentencesStart.get(i).getWeight();

            if(values.get(i)>0){
                scoredSentences.add(new ScoredSentence(id,value,weight,true));
            }
            else{
                scoredSentences.add(new ScoredSentence(id,value,weight,false));
            }
        }

        List<ScoredSentence> scoredSentences1=new ArrayList<>();

        for(ScoredSentence scoredSentence:scoredSentences){
            //re rank sentences based on their length
            String[] tokens=scoredSentence.getValue().split(" ");   //divide into tokens
            double coeff=Math.min(1,tokens.length/7d);  //if the sentence has more than x words...
            scoredSentences1.add(new ScoredSentence(scoredSentence.getId(), scoredSentence.getValue(), scoredSentence.getWeight() * coeff, scoredSentence.isPositive()));
        }
        startTime = System.currentTimeMillis();
        if(params.checkParam(Params.Param.IBM_PROJECT_DEBATER_EVIDENCE_DETECTION)){
            //TODO: CHECK EVIDENCE WITH PROJECT DEBATER
            List<ScoredSentence> scoredSentences2=new ArrayList<>();
            List<String> sentences=new ArrayList<>();
            for(ScoredSentence scoredSentence:scoredSentences) {
                sentences.add(scoredSentence.getValue());
            }
            List<Float> score=DebaterManager.getEvidenceScores(t.getTitle(),sentences,projectDebaterKey);

            for(int i=0;i<score.size();i++){
                scoredSentences2.add(new ScoredSentence(scoredSentences1.get(i).getId(),scoredSentences1.get(i).getValue(),
                        scoredSentences1.get(i).getWeight()*score.get(i),scoredSentences1.get(i).isPositive()));
            }
            scoredSentences1.clear();
            scoredSentences1.addAll(scoredSentences2);
        }


        endTime = System.currentTimeMillis();
        duration = (endTime - startTime);
        System.out.println("Evidence runtime: " + (duration/1000) + " seconds");

        List<ScoredSentence> positiveSentences=new ArrayList<>();
        List<ScoredSentence> negativeSentences=new ArrayList<>();
        for(ScoredSentence s:scoredSentences1){
            if(s.isPositive()){
                positiveSentences.add(s);
            }
            else{
                negativeSentences.add(s);
            }
        }

        positiveSentences.sort(Comparator.comparingDouble(ScoredSentence::getWeight));
        Collections.reverse(positiveSentences);  //now should be in ascending order!

        negativeSentences.sort(Comparator.comparingDouble(ScoredSentence::getWeight));
        Collections.reverse(negativeSentences);

        for(int pro=0,con=0,i=1;pro<positiveSentences.size()-1&&con<negativeSentences.size()-1;i++){
            double proWeight=positiveSentences.get(pro).getWeight()+positiveSentences.get(pro+1).getWeight();
            double conWeight=negativeSentences.get(con).getWeight()+negativeSentences.get(con+1).getWeight();
            if(proWeight>=conWeight){
                run.printf(Locale.ENGLISH, "%s %s %s,%s %d %.6f %s%n", t.getNumber(),"PRO", positiveSentences.get(pro).getId(),
                        positiveSentences.get(pro+1).getId(), i, proWeight/2d, runID);
                pro+=2;
            }
            else{
                run.printf(Locale.ENGLISH, "%s %s %s,%s %d %.6f %s%n", t.getNumber(),"CON", negativeSentences.get(con).getId(),
                        negativeSentences.get(con+1).getId(), i, conWeight/2d, runID);
                con+=2;
            }

        }
    }

    /**
     * Class representing a Topic.
     * The topic can be processed with the Analyzer to obtain a tokenized title and description with
     * tokens that can be weighted differently according to some scoring functions.
     */
    private static class Topic{
        //These fields were used before when using QualityQuery
        //public static final String TITLE_FIELD="title";
        //public static final String DESCRIPTION_FIELD="description";
        //public static final String NARRATIVE_FIELD="narrative";

        private final String number;
        private final String title;
        private final String description;
        private final String narrative;

        /**
         * List of {@link TokenWeightTag} representing the title tokenized using the Analyzer without stemmer.
         * It is structured like this:
         * titleTokenizedWeighted.get(0).get(0) is the fist token in the title with its weight and tag
         * titleTokenizedWeighted.get(0).get(1) is the synonym for the first token in the title with its weight and tag
         * titleTokenizedWeighted.get(1).get(0) is the second token in the title with its weight and tag
         * titleTokenizedWeighted.get(1).get(1) is the synonym for the second token in the title with its weight and tag
         */
        private final List<List<TokenWeightTag>>titleTokenizedWeighted;

        /**
         * List of  {@link TokenWeightTag} representing the description tokenized using the Analyzer without stemmer.
         * It is structured like this:
         * titleTokenizedWeighted.get(0).get(0) is the fist token in the description with its weight and tag
         * titleTokenizedWeighted.get(0).get(1) is the synonym for the first token in the description with its weight and tag
         * titleTokenizedWeighted.get(1).get(0) is the second token in the description with its weight and tag
         * titleTokenizedWeighted.get(1).get(1) is the synonym for the second token in the description with its weight and tag
         */
        private final List<List<TokenWeightTag>>descriptionTokenizedWeighted;

        /**
         * Initializes the topic
         * @param number  the topic number
         * @param title  the topic title
         * @param description  the topic description
         * @param narrative  the topic narrative
         * @param titleTokenizedWeighted  the title tokenized and weighted
         * @param descriptionTokenizedWeighted  the description tokenized and weighted
         */
        public Topic(final String number,final String title,final String description,final String narrative, final List<List<TokenWeightTag>>titleTokenizedWeighted, final List<List<TokenWeightTag>>descriptionTokenizedWeighted){
            this.number=number;
            this.title=title;
            this.description=description;
            this.narrative=narrative;
            this.titleTokenizedWeighted=List.copyOf(titleTokenizedWeighted);
            this.descriptionTokenizedWeighted=List.copyOf(descriptionTokenizedWeighted);
        }

        /**
         * Gets the topic number
         * @return  the topic number
         */
        public String getNumber() {return number;}

        /**
         * Gets the topic title
         * @return  the topic title
         */
        public final String getTitle() {
            return title;
        }

        /**
         * Gets the topic description
         * @return  the topic description
         */
        public final String getDescription() {
            return description;
        }

        /**
         * Gets the topic narrative
         * @return  the topic narrative
         */
        public final String getNarrative() {
            return narrative;
        }

        /**
         * Gets the topic title tokenized and weighted
         * @return  the topic title tokenized and weighted
         */
        public final List<List<TokenWeightTag>> getTitleTokenizedWeighted(){
            if(titleTokenizedWeighted==null){
                return null;
            }
            return List.copyOf(titleTokenizedWeighted);
        }

        /**
         * Gets the topic description tokenized and weighted
         * @return  the topic description tokenized and weighted
         */
        public final List<List<TokenWeightTag>> getDescriptionTokenizedWeighted(){
            if(descriptionTokenizedWeighted==null){
                return null;
            }
            return List.copyOf(descriptionTokenizedWeighted);
        }
    }

    /**
     * Class representing a Sentence with its score and stance that is used to rank sentences accordingly.
     */
    private static class ScoredSentence{
        private final String id;
        private final String value;
        private final double weight;
        private final boolean positive;
        private final boolean positiveEvaluated;

        /**
         * Initializes this class
         * @param id  the id of the sentence
         * @param weight  the weight of the sentence
         */
        public ScoredSentence(String id, double weight){
            this.id=id;
            this.value=null;
            this.weight=weight;
            this.positive=false;
            this.positiveEvaluated=false;
        }

        /**
         * Initializes this class
         * @param id  the id of the sentence
         * @param value  the sentence text
         * @param weight  the weight of the sentence
         */
        public ScoredSentence(String id, String value, double weight){
            this.id=id;
            this.value=value;
            this.weight=weight;
            this.positive=false;
            this.positiveEvaluated=false;
        }

        /**
         * Initializes this class
         * @param id  the id of the sentence
         * @param value  the sentence text
         * @param weight  the weight of the sentence
         * @param positive  true if the sentence is classified as PRO towards a topic, false viceversa
         */
        public ScoredSentence(String id, String value, double weight,boolean positive){
            this.id=id;
            this.value=value;
            this.weight=weight;
            this.positive=positive;
            this.positiveEvaluated=true;
        }


        /**
         * Gets the sentence id
         * @return  the sentence id
         */
        public String getId() {
            return id;
        }

        /**
         * Gets the sentence text
         * @return  the sentence text
         */
        public String getValue() {
            return value;
        }

        /**
         * Gets the sentence weight
         * @return  the sentence weight
         */
        public double getWeight() {
            return weight;
        }

        /**
         * Gets if the sentence is PRO towards a topic
         * @return  true if the sentence is PRO towards a topic
         */
        public boolean isPositive() {
            if(!positiveEvaluated){
                throw new IllegalArgumentException();
            }
            return positive;
        }
    }


    private static class RawTopics {
        @JacksonXmlElementWrapper(localName = "topic")
        @JsonProperty("topic")
        private List<RawTopic> topics = new ArrayList<>();

        @JsonSetter
        public void setTopic(RawTopic topic) {
            this.topics.add(topic);
        }

        public List<RawTopic> getTopics(){
            return topics;
        }
    }

    private static class RawTopic{
        @JacksonXmlProperty
        private int number;
        @JacksonXmlProperty
        private String title;
        @JacksonXmlProperty
        private String description;
        @JacksonXmlProperty
        private String narrative;

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getNarrative() {
            return narrative;
        }

        public void setNarrative(String narrative) {
            this.narrative = narrative;
        }
    }

    /**
     * Just for testing purposes
     */
    public static void main(String[] args) throws Exception{
        final String topics = "PATH TO TOPICS";

        final String indexPath= "PATH TO INDEX";

        final String runPath = "runs";

        final String runID = "INTSEG-EnglishStemmer-";

        final int maxDocsRetrieved = 1000;

        final String configPath = "config";
        final Path configDir = Paths.get(configPath);

        //Analyzer used to index the documents
        final Analyzer a=  new IntsegAnalyzer(
                IntsegAnalyzer.TokenizerType.Standard, 2, 10,
                true, "stoplist-top100-sentence.txt", IntsegAnalyzer.StemFilterType.EnglishMinimal, null, null);

        //Analyzer used for the first tokenization phase to split sentence into tokens and retrieve synonyms and POS-tags
        final Analyzer analyzerWithoutStemmer=  new IntsegAnalyzer(
                IntsegAnalyzer.TokenizerType.Letter,  2, 10,
                true, null, IntsegAnalyzer.StemFilterType.EnglishMinimal, null, null);

        ISearcher s = new ISearcher(a,analyzerWithoutStemmer,new LMDirichletSimilarity(),indexPath,topics,50,
                runID,runPath,maxDocsRetrieved, configDir+"\\SearcherParams.xml");

        s.search();
    }
}
