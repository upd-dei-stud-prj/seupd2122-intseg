package it.unipd.dei.se.searcher;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * A class representing a token with its weight and tag
 *
 * @author Andrea Pasin
 */
public class TokenWeightTag {
    /**
     * The token
     */
    private final String token;

    /**
     * The weight associated to the token
     */
    private final float weight;

    /**
     * The pos-tag associated to the token
     */
    private final String tag;

    /**
     * The probability of correctness of the pos-tag associated to the token
     */
    private final double probability;

    /**
     * State field to mark when a {@link TokenWeightTag} is empty or not
     */
    private final boolean isEmpty;


    /**
     * Constructor for {@link TokenWeightTag}
     *
     * @param token  the token in a query
     * @param tag  the tag for the token
     * @param probability  the probability of the tag to be correct for the token
     * @param weight  the weight corresponding to the given token for a query
     * @throws IllegalArgumentException  if the provided weight is less than zero
     */
    public TokenWeightTag(@NotNull final String token, final float weight, @NotNull final String tag, final double probability){
        if(weight<0){
            throw new IllegalArgumentException("weight cannot be less than zero!");
        }
        this.token=token;
        this.weight=weight;
        this.tag=tag;
        this.probability=probability;
        this.isEmpty=false;
    }

    /**
     * Constructor for {@link TokenWeightTag}
     *
     * @param token  the token in a query
     * @param tag  the tag for the token
     * @param probability  the probability of the tag to be correct for the token
     */
    public TokenWeightTag(@NotNull final String token, @NotNull final String tag, final double probability){
        this.token=token;
        this.weight=1f;
        this.tag=tag;
        this.probability=probability;
        this.isEmpty=false;
    }


    /**
     * Constructor for {@link TokenWeightTag}
     *
     * @param token  the token in a query
     * @param weight  the weight corresponding to the given token for a query
     * @throws IllegalArgumentException  if the provided weight is less than zero
     */
    public TokenWeightTag(@NotNull final String token, final float weight){
        if(weight<0){
            throw new IllegalArgumentException("weight cannot be less than zero!");
        }
        this.token=token;
        this.weight=weight;
        this.tag=null;
        this.probability=0;
        this.isEmpty=false;
    }

    /**
     * Constructor for {@link TokenWeightTag}. The object created will be empty
     */
    public TokenWeightTag(){
        this.isEmpty=true;
        this.token=null;
        this.tag=null;
        this.probability=0;
        this.weight=1f;
    }

    /**
     * Returns the token associated to this class
     *
     * @return  the token associated to this class
     */
    public final String getToken() {
        return token;
    }

    /**
     * Returns the weight associated to this class
     *
     * @return  the weight associated to this class
     */
    public final float getWeight() {
        return weight;
    }

    /**
     * Checks if this class has been initialized with a token and a weight or not
     *
     * @return  true if the class has been initialized with the empty constructor
     */
    public final boolean isEmpty(){
        return isEmpty;
    }

    /**
     * Returns the tag associated to this class
     *
     * @return  the tag associated to this class
     */
    public final String getTag() {
        return tag;
    }

    /**
     * Returns the probability associated to this class
     *
     * @return  the probability associated to this class
     */
    public final double getProbability() {
        return probability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenWeightTag that = (TokenWeightTag) o;
        return Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token,tag);
    }

    @Override
    public String toString() {
        return "TokenWeightTag{" +
                "token='" + token + '\'' +
                ", weight=" + weight +
                ", tag='" + tag + '\'' +
                ", probability=" + probability +
                ", isEmpty=" + isEmpty +
                '}';
    }
}
