package it.unipd.dei.se.parser;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.List;
import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;

/**
 * Parser for the Touché Task 1
 *
 * @author Sepide Bahrami
 * @author Paria Tahan
 */
public class IntsegParser extends DocumentParser {


    /**
     * The currently parsed document.
     */
    private ParsedDocument document = null;
    /**
     * Creates a new document parser with Jackson CsvMapper
     *
     *
     * @throws NullPointerException     if {@code in} is {@code null}.
     * @throws IllegalArgumentException if any error occurs while creating the parser.
     */
    public IntsegParser(final String pathCollection) throws IOException{
        super(new CsvMapper(),pathCollection);

    }


    @Override
    public boolean hasNext() {
        String doc_id;
        String prem_stance = null;
        final LinkedHashMap<String, String> prem_id_body = new LinkedHashMap<>();
        final LinkedHashMap<String, String> conc_id_body = new LinkedHashMap<>();
        String context_field;
        try {
            if(!(in.hasNext())) return false;
            List<String> line= in.nextValue();

            /**
             * Get the value of the stance from the premises
             */
            String temp1 = line.get(2);

            if(temp1.contains("\'stance\': \'PRO")){
                prem_stance="PRO";
            }
            else if(temp1.contains("\'stance\': \'CON")){
                prem_stance="CON";
            }

            /**
             * Get the value of the Premise_ID and Conclusion_ID and their text from the sentences
             */
            String temp2 = line.get(4);
            //String original=line.get(2);//temp2=temp2.replaceAll("\\'[a-zA-Z 0-9\\.\\,\\+\\-]","");
            temp2=temp2.replaceAll("['\"\\\\]","");
            //temp2=temp2.replaceAll("\\\\","");
            temp2=temp2.replaceAll("sent_id","\"sent_id\"");
            temp2=temp2.replaceAll("sent_text","\"sent_text\"");
            //temp2=temp2.replaceAll("sent_text","\"sent_text\"");
            temp2=temp2.replaceAll("\": ","\": \"");
            temp2=temp2.replaceAll(", \"","\", \"");
            temp2=temp2.replaceAll("}, \\{\"","\"}, \\{\"");
            temp2=temp2.replaceAll("}]","\"}]");
            try {

                ObjectMapper mapper = new ObjectMapper();
                JsonNode df = mapper.readTree(temp2);
                if (df.isArray()) {
                    for (JsonNode node : df) {
                        String id = node.get("sent_id").asText();
                        String value = node.get("sent_text").asText();

                        if (id.contains("PREMISE")) {
                            prem_id_body.put(id, value);
                        } else {
                            conc_id_body.put(id, value);
                        }
                    }
                }

            }
            catch (JsonParseException ex){
                JsonLocation location=ex.getLocation();
                System.out.println(temp2.substring(location.getColumnNr()));
            }

            /**
             * Get the Document ID and Context
             */
            doc_id = line.get(0);
            context_field = line.get(3);

            if (doc_id != null)
                document = new ParsedDocument(doc_id, prem_id_body, prem_stance, conc_id_body, context_field);


        } catch (IOException e) {
            throw new IllegalStateException("Unable to parse the document.", e);
        }
        return next;
    }

    @Override
    protected final ParsedDocument parse() {
        return document;
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        int i=0;

        IntsegParser p = new IntsegParser("PATH TO CSV");
        long startTime = System.currentTimeMillis();
        for (ParsedDocument d : p) {
            i++;
            if(i%10000==0){
                long endTime = System.currentTimeMillis();
                long duration = (endTime - startTime);
                System.out.println("Parser runtime: " + (duration/1000) + " seconds. Documents read= "+i);
            }
        }
    }
}