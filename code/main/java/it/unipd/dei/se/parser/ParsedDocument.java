package it.unipd.dei.se.parser;

import java.util.LinkedHashMap;
import org.apache.lucene.document.Field;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Represents a parsed document to be indexed.
 */
public class ParsedDocument {
    /**
     * The names of the {@link Field}s within the index.
     */
    public final static class FIELDS {
        public static final String DOC_ID = "doc_id";
        public static final LinkedHashMap<String, String> PREM_ID_BODY = null;
        public static final String PREM_STANCE = "prem_stance";
        public static final LinkedHashMap<String, String> CONC_ID_BODY = null;
        public static final String Context_Field = "context";
    }

    private final String doc_id;
    private final LinkedHashMap<String, String> prem_id_body = new LinkedHashMap<>();
    private final String prem_stance;
    private final LinkedHashMap<String, String> conc_id_body = new LinkedHashMap<>();
    private final String context_field;

    /**
     * Creates a new parsed document
     *
     * @param inp_doc_id the unique document identifier.
     * @param inp_prem_id_body the hashtable of the premise sentence.
     * @param inp_prem_stance the unique premise stance.
     * @param inp_conc_id_body the hashtable of the conclusion sentence.
     * @param inp_context_field the context of the document.
     * @throws NullPointerException  if {@code inp_doc_id} and/or {@code inp_prem_id_body}
     *                                  {@code inp_prem_stance} and/or {@code inp_conc_id_body}
     *                                  are {@code null}.
     * @throws IllegalStateException if {@code inp_doc_id} and/or {@code inp_prem_id_body}
     *                                  {@code inp_prem_stance} and/or {@code inp_conc_id_body}
     *                                  are empty.
     */
    public ParsedDocument(final String inp_doc_id, final  LinkedHashMap<String, String> inp_prem_id_body,
                          final String inp_prem_stance, final  LinkedHashMap<String, String> inp_conc_id_body, final String inp_context_field) {
        if (inp_doc_id == null)
            throw new NullPointerException("Document identifier cannot be null.");
        if (inp_doc_id.isEmpty())
            throw new IllegalStateException("Document identifier cannot be empty.");
        this.doc_id = inp_doc_id;

        if (inp_prem_id_body == null)
            throw new NullPointerException("Premise hashtable cannot be null.");
        /*if (inp_prem_id_body.isEmpty())
            throw new IllegalStateException("Premise hashtable cannot be empty.");*/
        this.prem_id_body.putAll(inp_prem_id_body);

        if (inp_prem_stance == null)
            throw new NullPointerException("Premise stance cannot be null.");
        if (inp_prem_stance.isEmpty())
            throw new IllegalStateException("Premise stance cannot be empty.");
        this.prem_stance = inp_prem_stance;

        if (inp_conc_id_body == null)
            throw new NullPointerException("Conclusion hashtable cannot be null.");
        /*if (inp_conc_id_body.isEmpty())
            throw new IllegalStateException("Conclusion hashtable cannot be empty.");*/
        this.conc_id_body.putAll(inp_conc_id_body);
        
        if (inp_context_field == null)
            throw new NullPointerException("Context cannot be null.");
        if (inp_context_field.isEmpty())
            throw new IllegalStateException("Context cannot be empty.");
        this.context_field = inp_context_field;
    }

    //getters
    public String get_doc_id()
    {
        return doc_id;
    }
    public String get_prem_stance() {
        return prem_stance;
    }
    public String get_context() {
        return context_field;
    }
    public LinkedHashMap<String, String> get_prem_id_body(){
        return prem_id_body;
    }
    public LinkedHashMap<String, String> get_conc_id_body(){
        return conc_id_body;
    }

    @Override
    public final String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("document_identifier", doc_id)
                .append("premise_stance", prem_stance)
                .append("prem_hashtable", prem_id_body.toString())
                .append("conc_hashtable", conc_id_body.toString())
                .append("context_field", context_field);

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedDocument) && doc_id.equals(((ParsedDocument) o).doc_id));
    }

    @Override
    public final int hashCode() {
        return 37 * doc_id.hashCode();
    }
}
