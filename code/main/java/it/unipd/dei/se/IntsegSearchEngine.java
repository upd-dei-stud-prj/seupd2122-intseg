package it.unipd.dei.se;

import it.unipd.dei.se.analyzer.IntsegAnalyzer;
import it.unipd.dei.se.indexer.DirectoryIndexer;
import it.unipd.dei.se.searcher.ISearcher;
import it.unipd.dei.se.tools.ResourcesUtil;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The main class of our project
 *
 * @author Gnana Prakash Goli
 * @author Andrea Pasin
 */
public class IntsegSearchEngine {

    public static void main(String[] args) throws IOException {
        if(args.length==0||args[0]==null||args[1]==null||args[2]==null){
            throw new IllegalArgumentException("Please provide the following 3 paths as cli input: [path to collection] [path to topics] [path to index location]");
        }
        //the path to the collection file
        final String collectionPath=args[0];

        //the path to the topics file
        final String topics=args[1];

        //the path to the directory of indexed files
        final String pathIndex=args[2];

        final int expectedDocs = 365408;
        final String charsetName = "ISO-8859-1";

        final int ramBuffer = 256;  //Increase it for a faster indexer

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd___HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
        final String runID = "INTSEG-Run-"+dtf.format(now);

        final int maxDocsRetrieved = 1000;


        final String runPath = "runs";
        final String configPath = "config";
        final Path runDir = Paths.get(runPath);
        final Path configDir = Paths.get(configPath);
        if (!Files.exists(runDir)) {
            Files.createDirectory(runDir);
        }
        if (!Files.exists(configDir)) {
            Files.createDirectory(configDir);
            PrintWriter fileAnalyzer = new PrintWriter(configDir+"\\AnalyzerParams.xml");
            PrintWriter fileSearcher = new PrintWriter(configDir+"\\SearcherParams.xml");
            String analyzerParams=ResourcesUtil.getFileContentFromName(ResourcesUtil.ResourcePath.ANALYZER_PARAMS);
            String searcherParams=ResourcesUtil.getFileContentFromName(ResourcesUtil.ResourcePath.SEARCHER_PARAMS);
            fileSearcher.printf(searcherParams);
            fileAnalyzer.printf(analyzerParams);
            fileAnalyzer.flush();
            fileAnalyzer.close();
            fileSearcher.flush();
            fileSearcher.close();
            System.out.println("Working environment has been created.\n Now add a valid API-Key inside 'config/SearcherParams.xml' to allow the project to run.\n It's possible to change configuration of our system by modifying the files that are located under '/config'");
            return;
        }

        final Analyzer intsegAnalyzerContext=  new IntsegAnalyzer(configDir+"\\AnalyzerParams.xml",ResourcesUtil.ResourcePath.CONTEXT_STOPLIST);

        final Analyzer intsegAnalyzerSentence=  new IntsegAnalyzer(configDir+"\\AnalyzerParams.xml",ResourcesUtil.ResourcePath.SENTENCE_STOPLIST);

        DirectoryIndexer indexer = new DirectoryIndexer(intsegAnalyzerContext,intsegAnalyzerSentence, new LMDirichletSimilarity(), ramBuffer, pathIndex, collectionPath,
                charsetName, expectedDocs);

        indexer.index();

        //Analyzer with only EnglishMinimalStemmer at maximum (no more advanced stemming)!
        final Analyzer analyzerWithoutStemmer=  new IntsegAnalyzer(
                IntsegAnalyzer.TokenizerType.Letter, 2, 20,
                true, null, IntsegAnalyzer.StemFilterType.EnglishMinimal, null, null);

        //Pass firstly the analyzer with sentence stoplist and then the analyzer without stemmer
        ISearcher s = new ISearcher(intsegAnalyzerSentence,analyzerWithoutStemmer,new LMDirichletSimilarity(),pathIndex,topics,50,
                runID,runPath,maxDocsRetrieved, configDir+"\\SearcherParams.xml");

        s.search();

        //Call this method to apply the system to 2021 topics (Also the topic file must be set accordingly)
        //s.search2021Topics();
    }
}
